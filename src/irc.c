#include "config.h"

#include <errno.h>
#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "gnomeirc.h"
#include "formatting.h"

#include <glib.h>

#ifdef GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

gboolean SetupOpen = FALSE;

gint irc_config_init ( void )
{
	GlobalSettings = g_new0 ( SettingsRec, 1 );

	GlobalSettings->fname_normal =
		g_strdup("-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*");
	GlobalSettings->fname_bold =
		g_strdup("-adobe-helvetica-bold-r-normal--*-120-*-*-*-*-*-*");
	GlobalSettings->fname_italic =
		g_strdup("-adobe-helvetica-medium-o-normal--*-120-*-*-*-*-*-*");

	GlobalSettings->userinfo = g_new0 ( UserInfoRec, 1 );

	GlobalSettings->userinfo->nick = g_strdup ( DEFAULT_NICK );
	GlobalSettings->userinfo->username = g_strdup ( DEFAULT_USERN );
	GlobalSettings->userinfo->realname = g_strdup ( DEFAULT_REALN );

	GlobalSettings->default_quit = g_strdup(DEFAULT_QUIT);
	GlobalSettings->default_part = g_strdup(DEFAULT_PART);

	return (0);
}

void irc_config_deinit(void)
{
	if (!GlobalSettings) return;
  
	if (GlobalSettings->fname_normal) g_free(GlobalSettings->fname_normal);
	if (GlobalSettings->fname_bold) g_free(GlobalSettings->fname_bold);
	if (GlobalSettings->fname_italic) g_free(GlobalSettings->fname_italic);

	if (GlobalSettings->userinfo) {
		if (GlobalSettings->userinfo->nick)
			g_free(GlobalSettings->userinfo->nick);
		if (GlobalSettings->userinfo->username)
			g_free(GlobalSettings->userinfo->username);
		if (GlobalSettings->userinfo->realname)
			g_free(GlobalSettings->userinfo->realname);

		g_free(GlobalSettings->userinfo);
	}

	if (GlobalSettings->default_quit)
		g_free(GlobalSettings->default_quit);
	if (GlobalSettings->default_part)
	  g_free(GlobalSettings->default_part);

	g_free(GlobalSettings);
}

static int chan_addnick_comp ( NickRec *p1, NickRec *p2 )
{
	if (p1 == NULL) return (-1);
	if (p2 == NULL) return (1);

	if (isop(*(p1->nick)) && !isop(*(p2->nick))) return (-1);
	if (!isop(*(p1->nick)) && isop(*(p2->nick))) return (1);

	if (isvoice(*(p1->nick)) && !isvoice(*(p2->nick))) return(-1);
	if (!isvoice(*(p1->nick)) && isvoice(*(p2->nick))) return(1);

	return g_strcasecmp ( p1->nick, p2->nick );
}

NickRec *irc_chan_addnick ( ChannelRec *chan, gchar *nick )
{
	NickRec *newnick;

	g_return_val_if_fail ( chan != NULL, NULL );
	g_return_val_if_fail ( nick != NULL, NULL );

	newnick = g_new0 ( NickRec, 1 );

	newnick->nick = g_strdup ( nick );

	chan->Nicks = g_list_insert_sorted ( chan->Nicks, newnick,
		(GCompareFunc) chan_addnick_comp );

	irc_chan_nickupd ( chan );

	return (newnick);
}

static gint chan_find_comp ( ChannelRec *chan, gchar *name )
{
	if (chan == NULL) return (1);

	return (g_strcasecmp ( chan->Name, name ));
}

ChannelRec *irc_chan_find_by_name ( gchar *name )
{
	GList *matchentry;
	ChannelRec *match = NULL;

	g_return_val_if_fail ( (name != NULL), NULL );
	if (Channels == NULL) return (NULL);

	matchentry = g_list_find_custom ( Channels, name,
		(GCompareFunc)chan_find_comp );

	match = matchentry != NULL?(ChannelRec *)matchentry->data:NULL;

	return (match);
}

ChannelRec *irc_chan_find_in_serv ( gchar *name, ServerRec *server )
{
	GList *matchentry;
	ChannelRec *match = NULL;

	g_return_val_if_fail ( (name != NULL), NULL );
	if (Channels == NULL) return (NULL);

	matchentry = g_list_find_custom ( Channels, name,
		(GCompareFunc)chan_find_comp );

	match = matchentry != NULL?(ChannelRec *)matchentry->data:NULL;

	while ( (match->Server != server) && match != NULL ) {
		matchentry = g_list_find_custom ( Channels, name,
			(GCompareFunc)chan_find_comp );

		match = matchentry != NULL?(ChannelRec *)matchentry->data:NULL;
	}

	return (match);
}

gboolean irc_chan_is_selected ( ChannelRec *chan )
{
	g_return_val_if_fail ( chan != NULL, (-1) );
	return (gui_chan_is_selected ( chan->GUIData ) );
}

void irc_chan_join ( WindowRec *window, gchar *name )
{
	ChannelRec *chan;

	if ( (name == NULL) || (*name == '\0') ) return;

	chan = irc_chan_find_by_name ( name );

	if (chan != NULL) {
		irc_chan_select ( chan );
		return;
	}
	else {
		chan = irc_chan_new ( name );
		irc_window_addchan ( window != NULL?window:CurrentWindow,
			chan );
	}
}

ChannelRec *irc_chan_new ( gchar *name )
{
	ChannelRec *chan;

	chan = g_new0 ( ChannelRec, 1 );

	chan->ChanIDNum = LastChanID++;
	chan->Nicks = g_list_alloc ( );
	chan->Banlist = g_list_alloc ( );
	chan->Name = name != NULL?g_strdup ( name ):NULL;
	chan->Topic = NULL;
	chan->Server = DefaultServer;
	chan->Window = NULL;

	chan->GUIData = gui_chan_new ( chan );

	if ( Channels == NULL ) Channels = g_list_alloc ( );
	Channels = g_list_append ( Channels, chan );

	return (chan);
}
void irc_chan_print (	ChannelRec	*channel,
			TextLevel	level,
			guchar		*text, ... )
{
	guchar *string;
	va_list ap;
	WindowRec *window;

	if (channel == NULL) window = CurrentWindow;
	else window = channel->Window;

	string = g_new ( guchar, 1000 );

	va_start ( ap, text );

	(void) vsnprintf ( string, 999, text, ap );
	irc_text_print ( window, level, string );

	va_end ( ap );

	g_free ( string );
}

void irc_chan_nickupd ( ChannelRec *chan )
{
	gui_chan_nickupd ( chan );
}

void irc_chan_parsemode ( ChannelRec *chan, gchar *modestr )
{
	NickRec *nrec;
	gchar *ptr, *mode, type;
	gint modenum;

	type = '+';
	for (mode = get_param(&modestr); *mode != '\0'; mode++) {
		if (*mode == '+' || *mode == '-') {
			type = *mode;
			continue;
		}

		switch (*mode) {
		case 'b':
			ptr = get_param(&modestr);
			if (*ptr == '\0') break;

//			if (type == '+')
//				add_ban(chan, ptr, esendnick, time(NULL));
//			else
//				remove_ban(chan, ptr);
			break;

		case 'v':
			ptr = get_param(&modestr);
			if (*ptr != '\0') {
				/* dont mess with voice shit if the dude is opped */
				if ((nrec=irc_nick_find_in_chan(chan, ptr)) != NULL)  {
					if (*(nrec->nick) != '@')  {
//						gui_change_nick_mode(chan, ptr,
//							type == '+'?'+':' ');
//						nick_mode_change(chan, ptr,
//							type == '+'?'+':' ');
					}
				}
	 		}
			break;

		case 'o':
			ptr = get_param(&modestr);
			if (*ptr != '\0') {
//				gui_change_nick_mode(chan, ptr,
//					type == '+' ? '@' : ' ');
//				nick_mode_change(chan, ptr,
//					type == '+' ? '@' : ' ');
			}
			break;

		case 'l':
			if (type == '-')
				chan->limit = 0;
			else {
				ptr = get_param(&modestr);
				sscanf(ptr, "%d", &chan->limit);
			}
			break;

		 case 'k':
			ptr = get_param(&modestr);
			if (*ptr != '\0' || type == '-') {
				if (chan->key != NULL) {
					g_free(chan->key);
					chan->key = NULL;
				}
				if (type == '+')
					chan->key = g_strdup(ptr);
			}
			if (type == '+')
				chan->mode |= CHANMODE_KEY;
			else
				chan->mode &= ~CHANMODE_KEY;
			break;

		default:
			modenum = 0;
			switch (*mode) {
			case 'i':
				modenum = CHANMODE_INVITE;
				break;
			case 'm':
				modenum = CHANMODE_MODERATE;
				break;
			case 's':
				modenum = CHANMODE_SECRET;
				break;
			case 'p':
				modenum = CHANMODE_PRIVATE;
				break;
			case 'n':
				modenum = CHANMODE_NOMSGS;
				break;
			case 't':
				modenum = CHANMODE_OP_TOPIC;
				break;
			}
			if (type == '-')
				chan->mode &= ~modenum;
			else
				chan->mode |= modenum;
			break;
		}
	}
}

void irc_chan_part ( ChannelRec *chan )
{
  gchar *partmsg;

	if ( chan == NULL ) return;

	if ( Channels != NULL ) Channels = g_list_remove ( Channels, chan );
	if ( chan->Nicks != NULL ) g_list_free ( chan->Nicks );
	if ( chan->Banlist != NULL ) g_list_free ( chan->Banlist );
	if ( chan->Topic != NULL ) g_free ( chan->Topic );

	partmsg = g_strdup_printf("PART %s :%s", chan->Name, GlobalSettings->default_part);
	irc_cmd_send(chan->Server, partmsg);
	g_free(partmsg);

	irc_window_remchan ( chan );

	g_free ( chan->GUIData );
	g_free ( chan->Name );
	g_free ( chan );
}

void irc_chan_part_all ( GList *channels )
{
	if ( channels == NULL ) return;

	g_list_foreach ( channels, (GFunc) irc_chan_part, NULL );
}

void irc_chan_remnick ( ChannelRec *chan, gchar *nick )
{
	NickRec *rec;

	g_return_if_fail(chan != NULL);
	g_return_if_fail(nick != NULL);

	rec = irc_nick_find_in_chan ( chan, nick );

	if (rec == NULL) return;
	
	chan->Nicks = g_list_remove(chan->Nicks, rec );

	irc_chan_nickupd ( chan );

	g_free(rec->nick);
	if (rec->host) g_free(rec->host);
	g_free(rec);
}

void irc_chan_select ( ChannelRec *chan )
{
	if (chan == NULL) return;

	if (chan->Window != CurrentWindow)
		irc_window_select (chan->Window);

	if (CurrentChannel != NULL)
		gui_chan_deselect ( CurrentChannel->GUIData );

	gui_chan_select ( chan );

	CurrentChannel = chan->Window->CurrentChannel = chan;
	DefaultServer = chan->Server;

	gui_chan_settopic ( chan );
}

void irc_chan_settopic ( ChannelRec *chan, gchar *topic )
{
	g_return_if_fail ( chan != NULL );
	g_return_if_fail ( topic != NULL );

	if ( chan->Topic != NULL ) g_free ( chan->Topic );

	chan->Topic = g_strdup ( topic );

	gui_chan_settopic ( chan );
}

ErrorCode irc_cmd_parse ( gchar *command, ChannelRec *channel )
{
	CommandRec curcmd;
	gint count = 0;
	ErrorCode err;
	gchar *params;

	g_return_val_if_fail ( command, 0 );

	curcmd = Commands[count];

	params = strchr ( command, ' ' );

/* Aliasing stuff goes here */

	while ( (curcmd.cmd != NULL) ) {
		if (!g_strncasecmp ( curcmd.cmd, command, strlen (curcmd.cmd) )) {
			err = (curcmd.cmdfunc( params, channel ));
			return err;
		}
		else {
			count++;
			curcmd = Commands[count];
		}
	}

	return (ERR_CMD_NOTFOUND);
}

gint irc_cmd_send ( ServerRec *server, char *cmd )
{
	char str[512];
	int len;

	g_return_val_if_fail ( cmd != NULL, 2 );

	if (server == NULL || !server->connected) {
		irc_error_gen ( ERR_SRV_NOTCONNECTED );
		return (0);
	}

	len = 0;
	while (*cmd != '\0' && len < 510 ) {
		str[len] = *cmd++;
		len++;
	}

	str[len++] = 13;
	str[len] = 10;

	return (net_transmit ( server->handle, str, len ));
}

void irc_entry_parse ( ChannelRec *channel, gchar *string )
{
	gchar		*str,
			*temp;
	ErrorCode	err;

	g_return_if_fail ( string != NULL );

	str = g_strdup ( string );

	temp = g_strstrip ( str );

	if ( *temp == '/' )
		temp = g_strdup ( ++temp );
	else
		temp = g_strdup_printf ( "say %s", str );
		
	err = irc_cmd_parse ( temp, channel!=NULL?channel:CurrentChannel );
	irc_error_gen ( err );

	g_free ( temp );
	g_free ( str );
}

void irc_error_gen ( ErrorCode err )
{
	switch (err) {
		case (ERR_NONE):
			break;
		case (ERR_UNKNOWN):
			irc_chan_print ( NULL, TLVL_ERROR,
				"An unknown error has occured" );
			break;
		case (ERR_CMD_NOTFOUND):
			irc_chan_print ( NULL, TLVL_ERROR,
				"Command not found" );
			break;
		case (ERR_CMD_FEWPARAMS):
			irc_chan_print ( NULL, TLVL_ERROR,
				"Not enough parameters to command" );
			break;
		case (ERR_CMD_BADPARAMS):
			irc_chan_print ( NULL, TLVL_ERROR,
				"Incorrect parameters to command" );
			break;
		case (ERR_CMD_NOCHANNEL):
			irc_chan_print ( NULL, TLVL_ERROR,
				"You are not in a channel" );
			break;
		case (ERR_SRV_NOTCONNECTED):
			irc_chan_print ( NULL, TLVL_ERROR,
				"You are not connected to a server" );
			break;
		case (ERR_SRV_CANTCONNECT):
			irc_chan_print ( NULL, TLVL_ERROR,
				"Cannot connect to specified server" );
			break;
	}
}

static int nick_find_comp ( NickRec *rec, gchar *nick )
{
	if (rec == NULL) return (1);
	if (nick == NULL) return (1);

	if (isircflag ( *(rec->nick) ))
		return g_strcasecmp ( (rec->nick + 1), nick );
	else
		return g_strcasecmp ( rec->nick, nick );
}

NickRec *irc_nick_find_in_chan ( ChannelRec *chan, gchar *nick )
{
	GList *tmp;

	g_return_val_if_fail ( chan != NULL, NULL );
	g_return_val_if_fail ( nick != NULL, NULL );

	if (isircflag(*nick)) nick++;

	tmp = g_list_find_custom ( chan->Nicks, nick,
		(GCompareFunc) nick_find_comp );

	return tmp!=NULL?tmp->data:NULL;
}

RootWindowRec *irc_root_new ( void )
{
	RootWindowRec	*rootwin;

	rootwin = g_new0 ( RootWindowRec, 1 );
	rootwin->WinList = g_list_alloc ( );

	rootwin->Server = DefaultServer;

	rootwin->WinIDNum = LastRootID++;
	rootwin->CurrentWindow = NULL;

	rootwin->title = g_strdup ( NAME );

	rootwin->GUIData = gui_root_new ( rootwin );

	if (RootWins == NULL)
		RootWins = g_list_alloc ( );

	RootWins = g_list_append ( RootWins, rootwin );

	return (rootwin);
}

void irc_root_print (	RootWindowRec	*root,
			TextLevel	level,
			guchar		*text, ... )
{
	guchar *string;
	va_list ap;
	WindowRec *window;

	if (root == NULL) window = CurrentWindow;
	else window = root->CurrentWindow;

	string = g_new ( guchar, 1000 );

	va_start ( ap, text );

	(void) vsnprintf ( string, 999, text, ap );
	irc_text_print ( window, level, string );

	va_end ( ap );

	g_free ( string );
}

static void server_parsecb ( ServerRec *server )
{
	gchar str[512];

	g_return_if_fail ( server != NULL );

	while (irc_server_recvline ( server, str ) > 0 )
		irc_server_parseline ( server, str );
}

static void server_tocb_1 ( ServerRec *server )
{
	gint ret;

	g_return_if_fail ( server != NULL );

	ret = net_testconnect ( server->handle );

	if (ret == -1) {
		irc_server_disconnect ( server );
		irc_server_delete ( server );
	}
}

static void server_connectcb_2 (	ServerRec		*server,
					gint			source,
					GuiInputCondition	condition )
{
	gchar tmp[512];
	gint i, j = sizeof(i);

	gui_input_remove(server->nbtag);

	if(getsockopt(source, SOL_SOCKET, SO_ERROR, &i, &j) == 0) {
		g_assert(j == sizeof(i));
		if(i == 0) {
			irc_server_print ( server, TLVL_SYSTEM,
				"Connection to %s established on port %d",
				server->host, server->port);
			server->handle = source;
			server->readtag = gui_input_add(source, GUI_INPUT_READ,
				(GuiInputFunc) server_parsecb, server);
			server->connected = 1;
			sprintf(tmp, "NICK %s", server->user->nick);
			irc_cmd_send(server, tmp);
			sprintf(tmp, "USER %s - - :%s", server->user->username,
				server->user->realname);
			irc_cmd_send(server, tmp);
			server->connected = 0;
			if (DefaultServer != NULL) DefaultServer = server;
			server->totag = gui_timeout_add ( 240000,
				(GuiTimeoutFunc) server_tocb_1, server );
			return;
		}
	}

	irc_server_print ( server, TLVL_SYSTEM, "Can't connect to %s on %d",
		server->host, server->port );
	net_disconnect(source);

	irc_server_delete ( server );
}

static void server_connectcb_1 (	ServerRec		*server,
					gint			source,
					GuiInputCondition	condition )
{
	gint fd, ip;
	gint p[2] = { server->nbpipe[0], server->nbpipe[1] };
	struct in_addr in;

	gui_input_remove ( server->nbtag );
	read ( source, &fd, sizeof(fd) );

	if (fd == -1) {
		irc_server_print ( server, TLVL_SYSTEM,
			"Can't connect to %s on port %d",
			server->host, server->port );
		irc_server_delete ( server );
	}
	else {
		read ( source, &ip, sizeof(ip));
		in.s_addr = ip;
		server->nbtag = gui_input_add ( fd, GUI_INPUT_WRITE,
			(GuiInputFunc) server_connectcb_2, server );
		irc_server_print ( server, TLVL_SYSTEM,
			"Connecting to %s (%s) on port %d",
			server->host, inet_ntoa(in), server->port );
	}

	close ( p[0] );
	close ( p[1] );
}

void irc_server_connect ( ServerRec *server )
{
	gint e;

	g_return_if_fail ( server != NULL );

	if ( pipe (server->nbpipe) < 0 ) {
#ifdef DEBUG
		g_error ( "Pipe could not be created!" );
#endif		
		irc_server_delete ( server );
		return;
	}

	e = net_nowait_connect_new ( server->host, server->port,
		server->nbpipe[1] );

	switch (e) {
		case (0):
			close ( server->nbpipe[0] );
			close ( server->nbpipe[1] );
			irc_server_delete ( server );
			irc_error_gen ( ERR_SRV_CANTCONNECT );
			return;
			break;
		case (1):
			server->nbtag = gui_input_add ( server->nbpipe[0],
				GUI_INPUT_READ,
				(GuiInputFunc) server_connectcb_1 , server );
			irc_server_print ( server, TLVL_SYSTEM,
				"Looking up %s", server->host );
			break;
		default:
			g_assert_not_reached();
	}
}

void irc_server_delete ( ServerRec *server )
{
	g_return_if_fail ( server != NULL );

#ifdef DEBUG
	if ( server->connected )
		g_error ( "Server %s is still connected, deleting anyway",
			server->host ); 
#endif

	g_free ( server->user->nick );
	g_free ( server->user->realname );
	g_free ( server->user->username );
	g_free ( server->user );
	g_free ( server->host );
	g_free ( server );

	server = NULL;
}

void irc_server_disconnect ( ServerRec *server )
{
        gchar *tmpqmsg;

	g_return_if_fail ( server != NULL );

	tmpqmsg = g_strdup_printf("QUIT :%s", GlobalSettings->default_quit);
	irc_cmd_send(server, tmpqmsg);
	g_free(tmpqmsg);

	if (server->handle != -1)
		net_disconnect ( server->handle );

	irc_window_print ( NULL, TLVL_SYSTEM, "Disconnected from %s",
		server->host );

	gui_input_remove ( server->readtag );
	gui_timeout_remove ( server->totag );

	if (DefaultServer == server)
		DefaultServer = NULL;

	server->connected = 0;
}

static gint server_find_comp ( ServerRec *server, gchar *name )
{
	if (server == NULL) return (1);

	return (g_strcasecmp ( server->host, name ));
}


ServerRec *irc_server_find_by_name ( gchar *name )
{
	GList *matchentry;
	ServerRec *match = NULL;

	g_return_val_if_fail ( (name != NULL), NULL );
	if (Servers == NULL) return (NULL);

	matchentry = g_list_find_custom ( Servers, name,
		(GCompareFunc)server_find_comp );

	match = matchentry != NULL?(ServerRec *)matchentry->data:NULL;

	return (match);
}

ServerRec *irc_server_new ( WindowRec *window, gchar *host, gint port )
{
	ServerRec *server;

	g_return_val_if_fail ( host != NULL, NULL );

	server = g_new0 ( ServerRec, 1 );

	server->host = g_strdup ( host );

	if (port <= 0)
		server->port = DEFAULT_PORT;
	else
		server->port = port;

	if (window == NULL)
		server->defwindow = CurrentWindow;
	else
		server->defwindow = window;

	server->user = g_new0 ( UserInfoRec, 1 );

	server->user->nick =
		g_strdup ( GlobalSettings->userinfo->nick );
	server->user->realname =
		g_strdup ( GlobalSettings->userinfo->realname );
	server->user->username =
		g_strdup ( GlobalSettings->userinfo->username );

	Servers = g_list_append(Servers, server);

	return (server);
}

gint irc_server_parseline ( ServerRec *server, gchar *str )
{
	gchar		*cmd;
	gint		cmdnum, n;
	WindowRec	*window;
	EventRec	*event;

	g_return_val_if_fail ( server != NULL, 0 );
	g_return_val_if_fail ( str != NULL, 0 );

	event = g_new0 ( EventRec, 1 );

	if (CurrentWindow == NULL || (CurrentWindow->CurrentChannel != NULL &&
	    CurrentWindow->CurrentChannel->Server == server) )
		window = CurrentWindow;
	else {
		if (server->defwindow != NULL)
			window = server->defwindow;
		else
			window = CurrentWindow;
	}

	event->server = server;
	event->server->defwindow = window;

	if (*str == ':') {
	  event->nick = g_strndup ( ++str, strchr(str+1, ' ')-str-1 );
	  if (strchr(event->nick, '!')) *(strchr(event->nick, '!')) = '\0';

		while (*str != '\0' && *str != ' ') {
			if (*str == '!') {
				*str = '\0';
				event->addr = g_strndup ( str + 1, strchr(str+1, ' ')-str-1 );
			}
			str++;
		}
		if (*str == ' ') {
			*(str++) = '\0';
			str = g_strchug(str);
		}
	}

	if (*str == '\0')
		return (1);

	cmd = str;
	while (*str != '\0' && *str != ' ') str++;
	if (*str == ' ') *(str++) ='\0';
	str = g_strchug(str);

	event->data = g_strdup ( str );

	cmdnum = -1;

	if (isdigit(*cmd)) {
		/* numeric command */
		if (sscanf(cmd, "%d", &cmdnum) != 1) cmdnum = -1;

		if (cmdnum < NUMEVENTS_MAX && NumericEvents[cmdnum] != NULL) {
			NumericEvents[cmdnum](event);
			return (1);
		}

		while (*str != '\0' && *str != ' ') str++;
		str = g_strchug(str);
	}
	else {
		/* named command */
		for (n = 0; NamedEvents[n].name != NULL; n++) {
			if (g_strcasecmp ( NamedEvents[n].name, cmd) == 0) {
				NamedEvents[n].func(event);
				return (1);
			}
		}
	}

	if (*str == ':')
		str++;
	else {
		gchar *ptr;

		ptr = strstr ( str, " :");
		if (ptr != NULL) {
			*ptr = '\0';

			irc_server_print ( event->server, TLVL_MSG, "%s %s\n",
				str, ptr+2 );
			return (1);
		}
	}

	irc_server_print ( event->server, TLVL_MSG, str );

	g_free ( event->addr );
	g_free ( event->nick );
	g_free ( event->data );
	g_free ( event );

	return (1);
}

void irc_server_print (	ServerRec	*server,
			TextLevel	level,
			guchar		*text, ... )
{
	guchar *string;
	va_list ap;
	WindowRec *window;

	if (server == NULL) window = CurrentWindow;
	else window = server->defwindow;

	string = g_new ( guchar, 1000 );

	va_start ( ap, text );

	(void) vsnprintf ( string, 999, text, ap );
	irc_text_print ( window, level, string );

	va_end ( ap );

	g_free ( string );
}

gint irc_server_recvline ( ServerRec *server, gchar *str )
{
	gint ret;

	g_return_val_if_fail ( server != NULL, (-1) );
	g_return_val_if_fail ( str != NULL, (-1) );

	ret = read_line (1, server->handle, str, server->buffer,
		sizeof ( server->buffer ), &server->bufferpos );

	if (ret == -1) {
		/* connection lost */
		irc_server_print ( server, TLVL_SYSTEM,
			"Connection lost to %s", server->host );

		irc_server_disconnect ( server );
		irc_server_delete ( server );
	}

	return (ret);
}

void irc_setup_apply ( )
{
}

void irc_setup_open ( )
{
	if (SetupOpen) {
		irc_window_print ( NULL, TLVL_ERROR, "Setup already open" );
		return;
	}

	SetupOpen = TRUE;
	gui_setup_open ( );
}

void irc_text_print ( WindowRec *win, TextLevel level, guchar *string )
{
	WindowRec *window;

	if ( win == NULL ) window = CurrentWindow;
	else window = win;

	switch (level) {
		case (TLVL_DCC):
			gui_text_winprint ( window, string );
			break;
		case (TLVL_ERROR):
			gui_text_winprintf ( window, FRMT_ERROR, string );
			break;
		case (TLVL_SYSTEM):
			gui_text_winprintf ( window, FRMT_SYSTEM, string );
			break;
		default:
			gui_text_winprint ( window, string );
			break;
	}
}

void irc_window_addchan ( WindowRec *window, ChannelRec *chan )
{
	g_return_if_fail ( window != NULL );
	g_return_if_fail ( chan != NULL );

	chan->Window = window;
	chan->Server = window->Server;

	window->Channels = g_list_append ( window->Channels, chan );

	gui_window_addchan ( window->GUIData, chan->GUIData );

	gui_window_labelupd ( window );

	irc_chan_select ( chan );
}

WindowRec *irc_window_new (  )
{
	WindowRec *window;

	window = g_new0 ( WindowRec, 1 );
	window->Channels = g_list_alloc ( );
	window->WinIDNum = LastWinID++;

	window->GUIData = gui_window_new ( window );

	if (Windows == NULL) Windows = g_list_alloc ( );

	Windows = g_list_append ( Windows, (gpointer) window );

	return (window);
}

void irc_window_print (	WindowRec	*win,
			TextLevel	level,
			guchar		*text, ... )
{
	guchar *string;
	va_list ap;
	WindowRec *window;

	if (win == NULL)
		window = CurrentWindow;
	else
		window = win;

	string = g_new ( guchar, 1000 );

	va_start ( ap, text );

	(void) vsnprintf ( string, 999, text, ap );
	irc_text_print ( window, level, string );

	va_end ( ap );

	g_free ( string );
}

void irc_window_remchan ( ChannelRec *chan )
{
	ChannelRec	*newchan;

	g_return_if_fail ( chan != NULL && chan->Window != NULL );

	chan->Window->Channels = g_list_remove ( chan->Window->Channels, chan );

	gui_chan_part ( chan );

	gui_window_labelupd ( chan->Window );

	if (chan == chan->Window->CurrentChannel)
		chan->Window->CurrentChannel = NULL;
	if (chan == CurrentChannel)
		CurrentChannel = NULL;

	newchan = (ChannelRec *)(g_list_first (chan->Window->Channels) )->data;

	irc_chan_select (newchan);
}

void irc_window_select ( WindowRec *window )
{
	g_return_if_fail ( window != NULL );

	CurrentWindow = window;
	window->Root->CurrentWindow = window;
	DefaultServer = window->Server;

	gui_window_select ( window );

	gui_chan_settopic ( window->CurrentChannel );
}
