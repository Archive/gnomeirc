#define NET_ERR_BADHOST 1
#define NET_ERR_BADSOCK 2

/* callback func type for network routines */
typedef void (*NetCallbackFunc) (int, void *);

/* thread information (used whether pthreads are enabled or not) */
struct __NetThreadRec {
  char *server;
  int port;
  NetCallbackFunc callback;
  gpointer data;
};
typedef struct __NetThreadRec NetThreadRec;

NetThreadRec *netthread_rec_new(char *server, int port, NetCallbackFunc callbaq, gpointer data);
void netthread_rec_free(NetThreadRec *rec);

int net_connect(char *server, int port, NetCallbackFunc callback, gpointer data);
void *net_connect_intermediary(NetThreadRec *rec);

int net_isten(char *addr, int port, NetCallbackFunc callback);
void net_listen_intermediary(gpointer data, gint socket, GdkInputCondition condition);

int net_read(int sock, char *buffer, int *len);
