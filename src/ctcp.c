/*

 ctcp.c : Functions handling CTCP events

    Copyright (C) 1999 Marcus Brubaker

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <string.h>
#include <stdlib.h>
#include <time.h>

#include "gnomeirc.h"

CTCPRec CTCPCmds[] =
{
	{ "VERSION", (CTCPFunc) ctcp_version },
	{ "PING", (CTCPFunc) ctcp_ping },
	{ "ACTION", (CTCPFunc) ctcp_action },
//	{ "DCC", (CTCPFunc) ctcp_dcc },
	{ "PAGE", (CTCPFunc) ctcp_page },
	{ "FINGER", (CTCPFunc) ctcp_finger },
	{ "TIME", (CTCPFunc) ctcp_time },
	{ "ECHO", (CTCPFunc) ctcp_echo },
	{ NULL, NULL }
};

/* CTCP reply sending function */
gint ctcp_timeout_send ( ServerRec *server )
{
	g_return_val_if_fail(server != NULL, 1);

	if (server->ctcpqueue == NULL) return 1;

	server->ctcpqueue = g_list_first(server->ctcpqueue);
	irc_cmd_send(server, (gchar *) server->ctcpqueue->data);

	server->ctcpqueue = g_list_remove(server->ctcpqueue, server->ctcpqueue);
	g_free(server->ctcpqueue->data);

	return 1;
}

/* Send CTCP reply with flood protection */
void ctcp_send_reply(ServerRec *server, gchar *data)
{
	g_return_if_fail(server != NULL);
	g_return_if_fail(data != NULL);

//	if (g_list_length(server->ctcpqueue) < MAX_CTCP_QUEUE)
		server->ctcpqueue = g_list_append(server->ctcpqueue,
			g_strdup(data));
}

/* CTCP version */
gint ctcp_version ( CTCPEventRec *event )
{
	gchar tmp[100];

	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1 );

	sprintf(tmp, "NOTICE %s :\001VERSION %s\001", event->sender, NAME);
	ctcp_send_reply(event->server, tmp);
	return 1;
}

/* CTCP ping */
gint ctcp_ping( CTCPEventRec *event )
{
	gchar tmp[512];

	g_return_val_if_fail(event->sender != NULL, 1);
	g_return_val_if_fail(event->data != NULL, 1);

	sprintf(tmp, "NOTICE %s :\001PING %s\001", event->sender, event->data);
	ctcp_send_reply(event->server, tmp);
	return 1;
}

/* CTCP action = /ME command */
gint ctcp_action( CTCPEventRec *event )
{
	ChannelRec	*chan;
	gchar		*target,
			*sender,
			*data;

	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1);
	g_return_val_if_fail(event->target != NULL, 1);
	g_return_val_if_fail(event->data != NULL, 1);

	target = g_strdup ( event->target );
	sender = g_strdup ( event->sender );
	data = g_strdup ( event->data );

	if (*target != '#' && *target != '&') {
		/* private or dcc action */
		if (*target == '=')
			irc_chan_print(
				irc_chan_find_in_serv (target, event->server),
				TLVL_MSG, FRMT_DCCACT, sender, data );
		else
			irc_chan_print(
				irc_chan_find_in_serv (target, event->server),
				TLVL_MSG, FRMT_MSGACT, sender, data);
	}
	else {
		/* channel action */
		chan = irc_chan_find_in_serv ( target, event->server );

		if (chan != NULL && chan->Window->CurrentChannel != NULL &&
		    g_strcasecmp(target, chan->Window->CurrentChannel->Name)==0)
			irc_chan_print( chan, TLVL_MSG, FRMT_CHANACT,
				sender, data);
		else
			irc_server_print(event->server, TLVL_MSG, FRMT_NCHANACT,
				sender, target, data);

/*
		if (chan != NULL && !chan->new_data &&
		    chan->Window != CurrentWindow) {
			chan->new_data = 1;
			gui_channel_hilight(chan);
		}
*/
	}

	g_free (target);
	g_free (sender);
	g_free (data);

	return 0;
}

/* CTCP DCC */
/*
gint ctcp_dcc( CTCPEventRec *event )
{
	g_return_val_if_fail(sender != NULL, 1);
	g_return_val_if_fail(data != NULL, 1);

	return dcc_handle_ctcp(sender, data);
}
*/

/* CTCP PAGE */
gint ctcp_page( CTCPEventRec *event )
{
	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1);

	irc_root_print (CurrentRoot, TLVL_MSG, CTCP_PAGE, event->sender,
		((event->data != NULL)&&(strlen(event->data)>0))?
		event->data:"No message"); 

	return 0;
}

/* CTCP FINGER */
gint ctcp_finger( CTCPEventRec *event )
{
	gchar tmp[512];

	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1);

	sprintf(tmp, "NOTICE %s :\001FINGER %s\001", event->sender, NAME);
	ctcp_send_reply(event->server, tmp);

	return 1;
}

/* CTCP TIME */
gint ctcp_time( CTCPEventRec *event )
{
	struct tm *tim;
	time_t t;
	gchar tmp[512], tmp2[50];

	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1);
   
	t = time(NULL);
	tim = localtime(&t);
	sprintf(tmp2, "%s", asctime(tim));
	tmp2[strlen(tmp2)-1] = '\0';
	sprintf(tmp, "NOTICE %s :\001TIME %s\001", event->sender, tmp2);
	ctcp_send_reply(event->server, tmp);
   
	return 1;
}

/* CTCP ECHO */
gint ctcp_echo( CTCPEventRec *event )
{
	gchar *tmp;

	g_return_val_if_fail(event != NULL, 1);
	g_return_val_if_fail(event->sender != NULL, 1);

	tmp = g_malloc(strlen(event->data)+strlen(event->sender)+17);
	sprintf(tmp, "NOTICE %s :\001ECHO %s\001", event->sender, event->data);
	ctcp_send_reply(event->server, tmp);
	g_free(tmp);

	return 1;
}

/* CTCP reply */
gint ctcp_reply ( gchar *sender, gchar *data )
{
	gchar *ptr;

	g_return_val_if_fail(sender != NULL, 0);
	g_return_val_if_fail(data != NULL, 0);

	ptr = strchr(data, ' ');
	if (ptr != NULL) *ptr++ = '\0';
	else ptr = "";

	if (strcmp(data, "DCC") == 0)
		return 0/*dcc_reply(sender, ptr)*/;

	if (!strcmp(data, "PING") && atol(ptr) > 0)
		irc_root_print ( CurrentRoot, TLVL_MSG, CTCP_PING,
			sender, (int)(time(NULL)-atol(ptr)));
	else
		irc_root_print ( CurrentRoot, TLVL_MSG, CTCP_REPLY,
			data, sender, ptr);

	return 1;
}
