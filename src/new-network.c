#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <errno.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "net_hilevel.h"

#ifdef USE_PTHREADS
#include <pthread.h>
#endif

/* make a new NetThreadRec */
NetThreadRec *netthread_rec_new(char *server, int port, NetCallbackFunc callbaq, gpointer data)
{
  NetThreadRec *newthread;

  /* bad params */
  if (!server || !port) return NULL;

  newthread = g_new0(NetThreadRec, 1);
  newthread->server = g_strdup(server);
  newthread->port = port;
  newthread->callback = callbaq;
  newthread->data = data;

  return(newthread);
}

/* clean up a NetThreadRec */
void netthread_rec_free(NetThreadRec *rec)
{
  if (!rec) return;
  if (rec->server) g_free(rec->server);
  g_free(rec);
}



/* connect to a host, run the callback when done (with or without threads) */
int net_connect(char *server, int port, NetCallbackFunc callback, gpointer data)
{
  pthread_t newthread;
  NetThreadRec *newthread_info;

  /* pluck a thread rec from the tree, and if it turns out sour, *
   * throw it back                                               *
   * so to speak                                                 */
  if (!(newthread_info = netthread_rec_new(server, port, callback))) return(FALSE);

#ifdef USE_PTHREADS
  if (pthread_create(&newthread, NULL, &net_connect_intermediary, newthread_info)) return(TRUE);
#else
  net_connect_intermediary(newthread_info);
#endif

  return(TRUE);
}

/* intermediary function for connect thread to use *
 * it g_frees the rec it is passed on error,       *
 * otherwise passes it to the rec's callback       */
void *net_connect_intermediary(NetThreadRec *rec)
{
  struct hostent *hent;
  struct sockaddr_in addr;
  int sockz;

  /* bad rec */
  if (!rec) return;

  /* bad hostname, return -1 */
  if (!rec->server) {
    (*rec->callback)(-1, NET_ERR_BADHOST);
    netthread_rec_free(rec);
    return;
  }

  hent = gethostbyname(rec->server);
    
  /* if lookup fails, return */
  if (!hent) {
    (*rec->callback)(-1, NET_ERR_BADHOST);
    netthread_rec_free(rec);
    return;
  }

  /* if socket creation fails, return */
  if (sockz = socket(AF_INET, SOCK_STREAM, 0) < 0) {
    (*rec->callback)(-1, NET_ERR_BADSOCK);
    netthread_rec_free(rec);
    return;
  }

  memcpy(&addr.sin_addr.s_addr, &hent->h_addr, hent->h_length);
  addr.sin_port = htons(rec->port);
  addr.sin_family = AF_INET;

  /* if connect fails, return */
  if (!connect(sockz, (struct sockaddr *)&addr, sizeof(addr))) {
    (*rec->callback)(-1, errno);
    Netthread_rec_free(rec);
    return;
  }

  /* holy shit it worked! call the callback, giving the connected socket number */
  (*rec->callback)(sockz, rec);

  return;
}



/* open a listening port, for one connection, bound to addr:port *
 * returns a gdk identifier for gdk_input_remove                 */
int net_listen(char *addr, int port, NetCallbackFunc callback)
{
  struct sockaddr_in sin;
  int lsock;
  NetThreadRec *rec;

  /* if rec doesnt build successfully, return -1 */
  if (!(rec = netthread_rec_new(addr, port, callback))) return(-1);

  /* if socket messes up, return */
  if ((lsock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
    netthread_rec_free(rec);
    return(-1);
  }

  /* set up the sockaddr for local binding */
  sin.sin_family = AF_INET;
  sin.sin_port = htons(rec->port);

  /* if ip to bind to is given, use it, if not, use any */
  if (rec->server) sin.sin_addr.s_addr = inet_addr(rec->server);
  else sin.sin_addr = INADDR_ANY;

  /* bind the bitch */
  if (bind(lsock, (struct sockaddr *)&sin, sizeof(sin))) {
    netthread_rec_free(rec);
    return(-1);
  }

  /* set the socket up to listen for 1 incoming connection only */
  if (listen(lsock, 1)) {
    netthread_rec_free(rec);
    return(-1);
  }

  /* add a gdk event hook for this socket and return the gdk tag */
  return(gdk_input_add_full(lsock, GDK_INPUT_READ, (GdkInputFunction)net_listen_intermediary, rec, NULL));
}

/* this func frees the rec passed to it if something fails, *
 * otherwise passes to the rec's callback                   */
void net_listen_lntermediary(NetThreadRec *rec, gint socket, GdkInputCondition condition)
{
  int nsock, sinsize;
  struct sockaddr_in sin;

  /* if somethin aint coming IN, we aint interested */
  if (condition != GDK_INPUT_READ) return;

  sin.sin_family = AF_INET;
  sinsize = sizeof(sin);

  nsock = accept(socket, (struct sockaddr *)&sin, &sinsize);

  close(socket);

  /* accept the incoming connection */
  if (nsock < 0) {
    (*rec->callback)(-1, errno);
    netthread_rec_free(rec);
    return;
  }

  /* change the NetThreadRec to reflect the remote host's connection info */
  g_free(rec->server);
  rec->server = g_strdup(inet_ntoa(sin.sin_addr));
  rec->port = ntohs(sin.sin_port);

  /* run the callback, at last */
  (*rec->callback)(nsock, rec);

  return;
}



/* read data from a socket, g_malloc the buffer space and fill *
 * len with buffer length.  if error with args, returns -1, if *
 * recv error or no data, returns 0, if all ok, returns 1      */
int net_read(int sock, char *buffer, int *len)
{
  char btemp[1024];
  int ret;

  /* socket must be real, buffer must be NULL, len must not be NULL */
  if (sock < 0 || buffer || !len) return(-1);

  /* loop to receive data and append to buffer */
  for(ret = recv(sock, btemp, sizeof(btemp), MSG_PEEK); ret > 0; ret = recv(sock, btemp, sizeof(btemp), MSG_PEEK))
    {
      /* if error and no previous data, return 0, if data, just end the loop */
      if (ret < 0) {
	if (buffer) break;
	else return(0);
      }
      
      /* if buffer hasnt been created yet, allocate it, if it has, grow it */
      if (buffer) buffer = g_realloc(buffer, *len+ret);
      else buffer = g_malloc(ret);
      *len += ret;

      /* copy over the data to the allocated buffer */
      memcpy(buffer, btemp, ret);

      /* clear the queue of the data we just received (MSG_PEEKed at) */
      recv(sock, btemp, sizeof(btemp), 0);
    }

  return(TRUE);
}

/* read up to the first \n of data from the given socket queue */
int net_read_oneline(int sock, char *buffer, int *len)
{
  /* UNFINISHED */
}
