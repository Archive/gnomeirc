#include "config.h"

#include "gnomeirc.h"

#include <string.h>

#include <glib.h>

gboolean Exiting = FALSE;

ErrorCode cmd_addn ( gchar *params, ChannelRec *channel )
{
	irc_chan_addnick ( channel, params );
	return (ERR_NONE);
}

ErrorCode cmd_remn ( gchar *params, ChannelRec *channel )
{
	irc_chan_remnick ( channel, params );
	return (ERR_NONE);
}

CommandRec Commands[] =
{
	{ "REMN", (CommandFunc) cmd_remn },
	{ "ADDN", (CommandFunc) cmd_addn },
	{ "CLOSE", (CommandFunc) cmd_close },
	{ "CONNECT", (CommandFunc) cmd_connect },
	{ "DISCONNECT", (CommandFunc) cmd_disconnect },
	{ "EXIT", (CommandFunc) cmd_exit },
	{ "JOIN", (CommandFunc) cmd_query },
	{ "PART", (CommandFunc) cmd_part },
	{ "QUERY", (CommandFunc) cmd_query },
	{ "QUIT", (CommandFunc) cmd_exit },
	{ "QUOTE", (CommandFunc) cmd_raw },
	{ "RAW", (CommandFunc) cmd_raw },
	{ "SAY", (CommandFunc) cmd_say },
	{ "SERVER", (CommandFunc) cmd_connect },
	{ "SETUP", (CommandFunc) cmd_setup },
	{ "WINDOW", (CommandFunc) cmd_window },
	{ NULL, NULL }
};

/* Begin close command */
ErrorCode cmd_close_page ( gchar *params, ChannelRec *channel );
ErrorCode cmd_close_root ( gchar *params, ChannelRec *channel );

CommandRec Params_close[] =
{
	{ "ROOT", (CommandFunc) cmd_close_root },
	{ "PAGE", (CommandFunc) cmd_close_page },
	{ NULL, NULL }
};

ErrorCode cmd_close ( gchar *params, ChannelRec *channel )
{
	gchar		*str,
			*param2;
	gint		count = 0;
	CommandRec	curparam;

	if (params != NULL) str = g_strstrip ( g_strdup (params) );
	else str = g_strdup (params);

	if (str != NULL) param2 = strchr ( str, ' ' );
	else param2 = str;

	curparam = Params_close[count];

	while ( curparam.cmd != NULL ) {
		if (!g_strncasecmp ( curparam.cmd, str != NULL?str:"\0", strlen (curparam.cmd) ))
			return (curparam.cmdfunc ( param2, channel ));
		else {
			count++;
			curparam = Params_close[count];
		}
	}

	g_free ( str );

	return (cmd_close_page(param2, channel));
};

ErrorCode cmd_close_page ( gchar *params, ChannelRec *channel )
{
	if (channel == NULL)
		gui_window_close ( CurrentWindow );
	else
		gui_window_close ( channel->Window );

	return (ERR_NONE);
}

ErrorCode cmd_close_root ( gchar *params, ChannelRec *channel )
{
	if ( (channel == NULL) || (channel->Window) )
		gui_root_close ( CurrentRoot );
	else
		gui_root_close ( channel->Window->Root );

	return (ERR_NONE);
}
/* End close command */

/* Begin connect command */
static void connect_setserv ( WindowRec *window, ServerRec *server )
{
	if (window == NULL) return;

	if (window->Server == NULL)
		window->Server = server;
}

ErrorCode cmd_connect ( gchar *params, ChannelRec *channel )
{
	ServerRec *server;
	WindowRec *window;
	gchar *ptr;
	gint port;

	if (channel == NULL)
		window = CurrentWindow;
	else
		window = channel->Window;

	ptr = strchr ( params, ':' );
	if (ptr == NULL)
		port = 6667;
	else {
		*ptr = '\0';
		if (sscanf ( ptr+1, "%d", &port) != 1) port = 6667;
	}

	server = irc_server_new ( window, g_strstrip (params), port );
	irc_server_connect ( server );

	if (server == NULL) return ERR_UNKNOWN;

	g_list_foreach ( Windows, (GFunc) connect_setserv, server );

	return (ERR_NONE);
}
/* End connect command */

/* Begin disconnect command */
ErrorCode cmd_disconnect ( gchar *params, ChannelRec *channel )
{
	if ((params == NULL) || (*params =='\0')) {
		if (CurrentWindow->Server != NULL)
			irc_server_disconnect ( CurrentWindow->Server );
		else
			return (ERR_SRV_NOTCONNECTED);
	}
	else {
		ServerRec *server = irc_server_find_by_name ( g_strstrip ( params ) );
		if (server != NULL)
			irc_server_disconnect ( server );
		else
			return (ERR_SRV_NOTCONNECTED);
	}

	return (ERR_NONE);
}
/* End connect command */

/* Begin exit command */
ErrorCode cmd_exit ( gchar *params, ChannelRec *channel )
{
	GList *tmpserver;
/* Put other exit type functions here */

	Exiting = TRUE;

	GLIST_FOREACH(tmpserver, Servers) {
		irc_server_disconnect(tmpserver->data);
		irc_server_delete(tmpserver->data);
	}
	
	while (g_list_length(RootWins) > 1 && RootWins->data != NULL) {
		gui_root_close(RootWins->data);
	}

	gui_exit ( );
	return (ERR_NONE);
};
/* End exit command */

/* Begin part command */
ErrorCode cmd_part ( gchar *params, ChannelRec *channel )
{
	ChannelRec *chan;
	gchar *str, *nextparam, *param, *errormsg = NULL;

	if (params != NULL) str = g_strstrip ( g_strdup ( params ) );
	else str = NULL;

	if ( str == NULL ) {
		irc_chan_part ( channel != NULL?channel:CurrentChannel );
		return (ERR_NONE);
	}
	else {
		param = str;
		for ( nextparam = strchr ( param, ' ' ) ; nextparam != NULL ; nextparam = strchr ( param, ' ' ) ) {
			*nextparam = '\0';
			chan = irc_chan_find_by_name ( param );
			if (chan == NULL) {
				errormsg = g_strdup_printf (MSG_CHAN_NOSUCH, param );
				irc_chan_print ( channel, TLVL_ERROR, errormsg );
				g_free(errormsg);
			}
			else
				irc_chan_part ( chan );
      
			*nextparam = ' ';
			param = g_strchug ( nextparam );
		}

		if ( (param != NULL) && (*param != '\0') ) {
			chan = irc_chan_find_by_name ( param );
			if (chan == NULL) {
				errormsg = g_strdup_printf (MSG_CHAN_NOSUCH, param );
				irc_chan_print ( channel, TLVL_ERROR, errormsg );
				g_free(errormsg);
			}
			else
				irc_chan_part ( chan );
		}
	}

	g_free ( str );

	return (ERR_NONE);
}
/* End part command */

/* Begin query command */
ErrorCode cmd_query ( gchar *params, ChannelRec *channel )
{
	gchar	*str,
		*nextparam,
		*param,
	        *joinmsg;

	if (params != NULL) str = g_strdup ( g_strstrip ( params ) );
	else str = g_strdup ( params );

	if ( (str == NULL) || (*str == '\0') ) {
		if (str != NULL) g_free ( str );
		return (ERR_CMD_FEWPARAMS);
	}
	else {
		param = str;
		for ( nextparam = strchr ( param, ' ' ); (nextparam != NULL) ; nextparam = strchr ( param, ' ' ) ) {
			*nextparam = '\0';
			irc_chan_join ( channel != NULL?channel->Window:CurrentWindow, param );

			if (ischannel(*param)) {
			  joinmsg = g_strdup_printf("JOIN %s", param);
			  irc_cmd_send(channel != NULL?channel->Server:CurrentWindow->Server, joinmsg);
			  g_free(joinmsg);
			}

			*nextparam = ' ';
			param = g_strchug ( nextparam );
		}
		if ( (param != NULL) && (*param != '\0') )
		irc_chan_join ( channel != NULL?channel->Window:CurrentWindow, param );

		if (ischannel(*param)) {
		  joinmsg = g_strdup_printf("JOIN %s", param);
		  irc_cmd_send(channel != NULL?channel->Server:CurrentWindow->Server, joinmsg);
		  g_free(joinmsg);
		}

	}

	g_free ( str );

	return (ERR_NONE);
}
/* End query command */


/* Begin raw command */
ErrorCode cmd_raw(gchar *params, ChannelRec *channel)
{
  g_return_val_if_fail(params != NULL, ERR_UNKNOWN);

  params = g_strstrip(params);

  if (channel) {

    if (!channel->Server) return(ERR_SRV_NOTCONNECTED);
    else irc_cmd_send(channel->Server, params);

  } else {

    if (!CurrentWindow) return(ERR_SRV_NOTCONNECTED);
    else irc_cmd_send(CurrentWindow->Server, params);

  }

  return(ERR_NONE);
}
/* End raw command */


/* Begin say command */

ErrorCode cmd_say ( gchar *params, ChannelRec *channel )
{
        gchar /* *target, */ *output, *sendcmd;

	g_return_val_if_fail ( params != NULL, ERR_UNKNOWN );

	if ( channel == NULL)
		return (ERR_CMD_NOCHANNEL);

	params = g_strstrip ( params );

//	target = get_param(&params);

	/* FIXME: DCC goes here */

	if (!channel->Server->connected)
		return (ERR_SRV_NOTCONNECTED);

	if (ischannel(*(channel->Name))) {
		if (channel->Window->CurrentChannel != channel)
			output = g_strdup_printf ( FRMT_SAY_NACT,
				channel->Server->user->nick, channel->Name,
				params );
		else
			output = g_strdup_printf ( FRMT_SAY_ACT,
				channel->Server->user->nick, params );
	}
	else {
		output = g_strdup_printf ( FRMT_SAY_MSG,
			channel->Name, params );
	}

	sendcmd = g_strdup_printf ( "PRIVMSG %s :%s", channel->Name, params );
	irc_cmd_send ( channel->Server, sendcmd );
	g_free ( sendcmd );

	irc_chan_print ( channel, TLVL_MSG, output );
	g_free ( output );

	return (ERR_NONE);
}

/* End say command */

/* Begin setup command */
ErrorCode cmd_setup ( gchar *params, ChannelRec *channel )
{
	irc_setup_open ();
	return (ERR_NONE);
}
/* End setup command */

/* Begin window command */
ErrorCode cmd_window_new ( gchar *params, ChannelRec *channel );

CommandRec Params_window[] =
{
	{ "CLOSE", (CommandFunc)cmd_close },
	{ "NEW", (CommandFunc)cmd_window_new },
	{ NULL, NULL }
};

ErrorCode cmd_window ( gchar *params, ChannelRec *channel )
{
	gchar 		*str,
			*param2;
	gint		count = 0;
	CommandRec	curparam;

	if (params != NULL) str = g_strstrip ( g_strdup ( params ) );
	else str = g_strdup ( params );

	if (str != NULL) param2 = strchr ( str, ' ' );
	else param2 = str;

	curparam = Params_window[count];

	while ( curparam.cmd != NULL ) {
		if (!g_strncasecmp ( curparam.cmd, str != NULL?str:"\0", strlen (curparam.cmd) )) {
			return (curparam.cmdfunc ( param2, channel ));
		}
		else {
			count++;
			curparam = Params_window[count];
		}
	}

	g_free ( str );

	return (ERR_CMD_BADPARAMS);
}

ErrorCode cmd_window_new_page ( gchar *params, ChannelRec *channel );
ErrorCode cmd_window_new_root ( gchar *params, ChannelRec *channel );

CommandRec Params_window_new[] =
{
	{ "PAGE", (CommandFunc)cmd_window_new_page },
	{ "ROOT", (CommandFunc)cmd_window_new_root },
	{ NULL, NULL }
};

ErrorCode cmd_window_new ( gchar *params, ChannelRec *channel )
{
	gchar		*str,
			*param2;
	gint		count = 0;
	CommandRec	curparam;

	if (params != NULL) str = g_strstrip ( g_strdup ( params ) );
	else str = g_strdup ( params );

	if (str != NULL) param2 = strchr ( str, ' ' );
	else param2 = str;

	curparam = Params_window_new[count];

	while ( curparam.cmd != NULL ) {
		if (!g_strncasecmp ( curparam.cmd, str != NULL?str:"\0", strlen (curparam.cmd) )) {
			return (curparam.cmdfunc ( param2, channel ));
		}
		else {
			count++;
			curparam = Params_window_new[count];
		}
	}

	g_free ( str );

	return (cmd_window_new_page(param2, channel));
}

ErrorCode cmd_window_new_page ( gchar *params, ChannelRec *channel )
{
	if ( (channel == NULL) || (channel->Window == NULL) )
		gui_root_addwindow ( CurrentRoot, irc_window_new () );
	else
		gui_root_addwindow ( channel->Window->Root, irc_window_new () );
	return (ERR_NONE);
}

ErrorCode cmd_window_new_root ( gchar *params, ChannelRec *channel )
{
	gui_root_create ( );

	return (ERR_NONE);
}

/* End window command */
