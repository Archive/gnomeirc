#include "config.h"

#include <errno.h>

#include "gnomeirc.h"

#include <glib.h>

gchar *get_param ( gchar **data )
{
	gchar *pos;

	g_return_val_if_fail(data != NULL, NULL);
	g_return_val_if_fail(*data != NULL, NULL);

	if (**data == ':') {
		pos = *data;
		*data += strlen(*data);
		return pos+1;
	}

	pos = *data;
	while (**data != '\0' && **data != ' ') (*data)++;
	if (**data == ' ') *(*data)++ = '\0';

	return pos;
}

gchar *get_params(gchar *data, gint count, ...)
{
	gchar **str, *tmp;

	va_list args;
	va_start(args, count);

	while (count-- > 0) {
		str = (gchar **) va_arg(args, gchar **);
		tmp = get_param(&data);
		if (str != NULL) *str = tmp;
	}
	va_end(args);

	return data;
}

gint ischannel ( gchar chr )
{
	return ( (chr == '#') || (chr == '&') );
}

gint isircflag ( gchar chr )
{
	return ( (chr == '@') || (chr == '+') || (chr == '-') || (chr == '~') );
}

gint isop( gchar chr )
{
	return(chr == '@');
}

gint isvoice( gchar chr )
{
	return(chr == '+');
}

/* Read line from somewhere..

    socket : 0 = read from file/pipe, 1 = read from socket
    handle : file/socket handle to read from
    str    : where to put line
    buf    : temp buffer
    bufsize: temp buffer size
    bufpos : current position in temp buffer
*/
gint read_line ( gint socket, gint handle, gchar *str, gchar *buf, gint bufsize, gint *bufpos )
{
	gint len, pos, bufs;

	if (handle == -1 || str == NULL ||
	    buf == NULL || bufpos == NULL)
		return -1;

	*str = '\0';

	if (socket) {
		len = net_receive(handle, buf+*bufpos, bufsize-*bufpos);
	}
	else {
		len = read(handle, buf+*bufpos, bufsize-*bufpos);
	}
	if (len < 0 && *bufpos != 0) {
		/* error/connection lost but still something in buffer.. */
		for (pos = 0; pos < *bufpos; pos++) {
			if (buf[pos] == 13 || buf[pos] == 10) {
				len = 0;
				break;
			}
		}
	}

	if (len < 0) {
		if (errno == EAGAIN || errno == EWOULDBLOCK) return 0;
		return -1;
	}

	bufs = len+*bufpos;
	if (bufs == 0) return 0; /* nothing came.. */

	for (pos = 0; pos < bufs; pos++) {
		if (buf[pos] == 13 || buf[pos] == 10) {
			/* end of line */
			memcpy(str, buf, pos); str[pos] = '\0';

			if (buf[pos] == 13 && pos+1 < bufs && buf[pos+1] == 10) pos++;

			memmove(buf, buf+pos+1, bufs-(pos+1));
			*bufpos = bufs-(pos+1);
			return 1;
		}
	}

	if (len+*bufpos == bufsize) {
		/* buffer overrun... */
		memcpy(str, buf, bufsize-1); str[bufsize-1] = '\0';
		*bufpos = 0;
		return 1;
	}

	/* EOL wasn't found, wait for more data.. */
	*bufpos = bufs;
	return 0;
}
