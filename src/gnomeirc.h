/*
 gnomeirc.h : 

    Copyright (C) 1998 Marcus Brubaker and Ben Pierce

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef __GNOMEIRC_H
#define __GNOMEIRC_H

#include "config.h"
#include "formatting.h"

#ifdef GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

#include <glib.h>

#include <stdio.h>

#define NAME			PACKAGE " " VERSION

#define CHANQUERY_BANLIST	0x01
#define CHANQUERY_WHO		0x02
#define CHANQUERY_MODE		0x04

#define CHANMODE_INVITE		0x01
#define CHANMODE_SECRET		0x02
#define CHANMODE_PRIVATE	0x04
#define CHANMODE_MODERATE	0x08
#define CHANMODE_NOMSGS		0x10
#define CHANMODE_OP_TOPIC	0x20
#define CHANMODE_KEY		0x40

#define DEBUG			1

#define DEFAULT_NICK		"Nietzsche"
#define DEFAULT_PORT		6667
#define DEFAULT_REALN		"No"
#define DEFAULT_USERN		"spoon"
#define DEFAULT_QUIT            "nut="
#define DEFAULT_PART            "net="
#define DEFAULT_NICKLIST_SIZE   85

#define GLIST_FOREACH(a, b)	for (a = g_list_first(b);a != NULL;a = a->next)

#define GUI_INPUT_READ		GDK_INPUT_READ
#define GUI_INPUT_WRITE		GDK_INPUT_WRITE

#define NUMEVENTS_MAX		600

#define WALLOP_TYPE_STD		0 /* nick,nick,nick type wallops */
#define WALLOP_TYPE_UNET	1 /* @#chan type wallops */

#define gui_input_add(a,b,c,d)	gdk_input_add(a,b,c,d)
#define gui_input_remove(a)	gdk_input_remove(a)
#define gui_timeout_add(a,b,c)	gtk_timeout_add(a,b,c)
#define gui_timeout_remove(a)	gtk_timeout_remove(a)

/* Typedefs Galore */
typedef	enum	_ErrorCode		ErrorCode;
typedef enum	_SetupStyle		SetupStyle;
typedef enum	_TextLevel		TextLevel;

typedef struct	_EventRec		EventRec;
typedef struct	_EventEntryRec		EventEntryRec;
typedef struct	_CommandRec		CommandRec;
typedef struct	_CTCPRec		CTCPRec;
typedef struct	_CTCPEventRec		CTCPEventRec;
typedef struct	_NickRec		NickRec;
typedef struct	_SettingsRec		SettingsRec;
typedef struct	_SetupSectionRec	SetupSectionRec;
typedef struct	_SetupBuildRec		SetupBuildRec;
typedef struct	_MenuItemRec		MenuItemRec;
typedef struct	_CommandHistory		CommandHistory;
typedef struct	_ChannelGUIData		ChannelGUIData;
typedef struct	_UserInfoRec		UserInfoRec;
typedef struct	_ServerRec		ServerRec;
typedef struct	_ChannelRec		ChannelRec;
typedef struct	_WindowGUIData		WindowGUIData;
typedef struct	_RootWindowGUIData	RootWindowGUIData;
typedef struct	_WindowRec		WindowRec;
typedef struct	_RootWindowRec		RootWindowRec;

typedef ErrorCode	(*CommandFunc)		( gchar *, ChannelRec * );
typedef gint		(*CTCPFunc)		( CTCPEventRec * );
typedef void		(*EventCallback)	( EventRec * );
typedef void		(*NetCallback)		( gint, gpointer );
typedef SetupBuildRec *	(*SetupInitFunc)	( gchar *, gint );

typedef GdkInputFunction	GuiInputFunc;
typedef GdkInputCondition	GuiInputCondition;
typedef GtkFunction		GuiTimeoutFunc;

enum _ErrorCode {
	ERR_NONE = 0,
	ERR_UNKNOWN,
	ERR_CMD_NOTFOUND,
	ERR_CMD_FEWPARAMS,
	ERR_CMD_BADPARAMS,
	ERR_CMD_NOCHANNEL,
	ERR_SRV_NOTCONNECTED,
	ERR_SRV_CANTCONNECT
};

enum _SetupStyle {
	SETUP_STYLE_PAGE,
	SETUP_STYLE_MENU,
	SETUP_STYLE_BOTH
};

enum _TextLevel {
	TLVL_DCC,
	TLVL_ERROR,
	TLVL_LOG, /* Logging...for later implementation */
	TLVL_MSG,
	TLVL_CHAN,
	TLVL_NOTICE,
	TLVL_SERVER,
	TLVL_SYSTEM,
};

struct _CTCPRec {
	gchar		*ctcp;
	CTCPFunc	func;
};

struct _CTCPEventRec {
	gchar		*sender,
			*target,
			*data;
	ServerRec	*server;
};

struct _NickRec {
	gchar	*nick,
		*host;
};

struct _EventRec {
	gchar		*data, /* data sent */
			*nick, /* nick of sender */
			*addr; /* address of sender */
	ServerRec	*server; /* server of sender */
};

struct _EventEntryRec {
	gchar *name;
	EventCallback func;
};

struct _CommandRec {
	gchar *cmd;
	CommandFunc cmdfunc;
};

struct _SettingsRec {
	GdkColor	*FGColor,
			*BGColor;
	gchar		*fname_normal,
			*fname_bold,
			*fname_italic;
	UserInfoRec	*userinfo;
        gchar           *default_quit,
	                *default_part;
};

struct _SetupSectionRec {
	gchar 		*name;
	SetupInitFunc	initfunc;
};

struct _SetupBuildRec {
	GtkWidget	*page,
			*label;
	MenuItemRec	*menuinfo;
};

struct _MenuItemRec {
	gchar 		*label,
			*hint,
			*command;
};

struct _UserInfoRec {
	gchar	*nick,
		*realname,
		*username,
		*host;

	/* mode[0] : Invisible        *
	 * mode[1] : Wallops          *
	 * mode[2] : Warning messages */
	gchar	mode[3];
};

struct _ServerRec {
	gboolean	connected;
	gchar		*host,
			buffer[512];
	gint		port,
			socket,
			handle,
			bufferpos,
			readtag,
			nbpipe[2],
			nbtag,
			totag,
			ison_reqs,
			wallop_type;
	GList		*ison_users,
			*ison_tempusers,
			*ctcpqueue;
	UserInfoRec	*user; /* User info needs to be tied to servers */
	WindowRec	*defwindow;
};

struct _ChannelRec {
	gint		ChanIDNum, /* Channel Identification Number */
			queries,
			mode,
			limit;
	gchar		*Name, /* Channel Name */
			*Topic, /* Channel Topic */
			*key; /* Channel Key */

	GList		*Nicks, /* List of Nicks in Channel */
			*Banlist; /* List of Bans in Channel */

	ServerRec	*Server;

	WindowRec	*Window;

	ChannelGUIData	*GUIData;

};

struct _ChannelGUIData {
	GtkWidget	*Nicklist, /* List of Nicknames */
			*Nickscroll, /* scrolled container for nicklist */
			*Button; /* Channel Button */
};

struct _WindowRec {
	gint		WinIDNum; /* Window Identification Number */

	GList		*Channels; /* List of Channels in the Window */


	ChannelRec	*CurrentChannel;

	ServerRec	*Server;

	RootWindowRec	*Root;

	WindowGUIData	*GUIData;
};

struct _WindowGUIData {
	GtkWidget	*Entry, /* Text Entry Line, Entry widget */
			*Text, /* Area for Showing Text */
			*Textbox, /* Box for Text widget and Scrollbar widget */
	                *MainPane, /* window pane for text window / nicklist */
	                *Nickbox,
			*Nicklist, /* List of Nicknames, points to Channels NickList */
			*Scrollbar, /* Window scrollbar */
			*Label, /* Notebook label */
			*BtnBox, /* Box for chan/query buttons */
			*Box; /* Box to pack widgets in */
};

struct _RootWindowRec {
	gint			WinIDNum; /* Window Identification Number */

	gchar			*title;

	GList			*WinList; /* List of Windows Within This One */

	RootWindowGUIData	*GUIData;

	ServerRec		*Server;

	WindowRec		*CurrentWindow;
};

struct _RootWindowGUIData {
#ifndef GNOME
	GtkWidget	*MainBox, /* Main box for packing stuff into */
			*StatusBar; /* Status Bar, points to Windows StatusBar */

	GtkItemFactory	*Menubar; /* Menubar, ItemFactory widget */
#endif
	GtkWidget	*Notebook, /* Notebook widget, page for each window inside */
			*app; /* Main Widget for the window, GnomeApp ifdef GNOME */

};

/* In commands.c */
ErrorCode cmd_close ( gchar *params, ChannelRec *channel );
ErrorCode cmd_connect ( gchar *params, ChannelRec *channel );
ErrorCode cmd_disconnect ( gchar *params, ChannelRec *channel );
ErrorCode cmd_exit ( gchar *params, ChannelRec *channel );
ErrorCode cmd_part ( gchar *params, ChannelRec *channel );
ErrorCode cmd_query ( gchar *params, ChannelRec *channel );
ErrorCode cmd_raw ( gchar *params, ChannelRec *channel );
ErrorCode cmd_say ( gchar *params, ChannelRec *channel );
ErrorCode cmd_setup ( gchar *params, ChannelRec *channel );
ErrorCode cmd_window ( gchar *params, ChannelRec *channel );


/* In ctcp.c */
gint ctcp_action ( CTCPEventRec *event );
gint ctcp_dcc ( CTCPEventRec *event );
gint ctcp_echo ( CTCPEventRec *event );
gint ctcp_finger ( CTCPEventRec *event );
gint ctcp_page ( CTCPEventRec *event );
gint ctcp_ping ( CTCPEventRec *event );
gint ctcp_time ( CTCPEventRec *event );
gint ctcp_version ( CTCPEventRec *event );

gint ctcp_reply ( gchar *sender, gchar *data );
void ctcp_send_reply ( ServerRec *server, gchar *data );
gint ctcp_timeout_send ( ServerRec *server );


/* In events-named.c */
void events_named_privmsg( EventRec *event );
void events_named_notice( EventRec *event );
void events_named_nick( EventRec *event );
void events_named_join( EventRec *event );
void events_named_part( EventRec *event );
void events_named_ping( EventRec *event );
void events_named_pong( EventRec *event );
void events_named_quit( EventRec *event );
void events_named_mode( EventRec *event );
void events_named_kick( EventRec *event );
void events_named_invite( EventRec *event );
void events_named_new_topic( EventRec *event );
void events_named_error( EventRec *event );
void events_named_wallops( EventRec *event );


/* In events-numeric.c */
void events_numeric_init ( void );

void events_numeric_welcome(EventRec *event);
void events_numeric_connected(EventRec *event);
void events_numeric_ison(EventRec *event);
void events_numeric_nick_in_use(EventRec *event);
void events_numeric_nick_unavailable(EventRec *event);
void events_numeric_names_list(EventRec *event);
void events_numeric_end_of_names(EventRec *event);
void events_numeric_topic(EventRec *event);
void events_numeric_topic_info(EventRec *event);
void events_numeric_who(EventRec *event);
void events_numeric_end_of_who(EventRec *event);
void events_numeric_ban_list(EventRec *event);
void events_numeric_end_of_banlist(EventRec *event);
void events_numeric_too_many_channels(EventRec *event);
void events_numeric_channel_is_full(EventRec *event);
void events_numeric_invite_only(EventRec *event);
void events_numeric_banned(EventRec *event);
void events_numeric_bad_channel_key(EventRec *event);
void events_numeric_bad_channel_mask(EventRec *event);
void events_numeric_channel_mode(EventRec *event);
void events_numeric_channel_created(EventRec *event);
void events_numeric_away(EventRec *event);
void events_numeric_unaway(EventRec *event);
void events_numeric_whois(EventRec *event);
void events_numeric_whois_idle(EventRec *event);
void events_numeric_whois_server(EventRec *event);
void events_numeric_whois_oper(EventRec *event);
void events_numeric_whois_channels(EventRec *event);
void events_numeric_end_of_whois(EventRec *event);


/* In gui.c */
void gui_exit ( void );
void gui_init ( gint argc, gchar *argv[] );
RootWindowRec *gui_root_init ( void );

void gui_chan_deselect ( ChannelGUIData *GUIData );
gboolean gui_chan_is_selected ( ChannelGUIData *GUIData );
ChannelGUIData *gui_chan_new ( ChannelRec *chan );
void gui_chan_nickupd ( ChannelRec *channel );
void gui_chan_part ( ChannelRec *chan );
void gui_chan_select ( ChannelRec *chan );
void gui_chan_settopic ( ChannelRec *chan );

void gui_root_addwindow ( RootWindowRec *rootwin, WindowRec *window );
void gui_root_close ( RootWindowRec *rootwin );
/* This is a pretty high level call, it may get moved sometime */
void gui_root_create ( void );
RootWindowGUIData *gui_root_new ( RootWindowRec *rootwin );
void gui_root_renum ( void );
void gui_root_settitle ( RootWindowGUIData *GUIData, gchar *title );

void gui_setup_open ( void );

void gui_text_print ( guchar *string );
void gui_text_printf ( guchar *format, ...);
gboolean gui_text_sbdown ( WindowRec *window );
void gui_text_setsbdown ( WindowRec *window );
void gui_text_winprint ( WindowRec *window, guchar *string );
void gui_text_winprintf ( WindowRec *window, guchar *format, ... );

void gui_window_addchan ( WindowGUIData *window, ChannelGUIData *chan );
void gui_window_close ( WindowRec *window );
void gui_window_labelupd ( WindowRec *window );
WindowGUIData *gui_window_new ( WindowRec *window );
void gui_window_renum ( void );
void gui_window_select ( WindowRec *window );


/* In gui_setup.c */
SetupBuildRec *setup_server ( gchar *name, SetupStyle style );


/* In irc.c */
gint irc_config_init ( void );
void irc_config_deinit(void);

NickRec *irc_chan_addnick ( ChannelRec *chan, gchar *nick );
ChannelRec *irc_chan_find_by_name ( gchar *name );
ChannelRec *irc_chan_find_in_serv ( gchar *name, ServerRec *server );
gboolean irc_chan_is_selected ( ChannelRec *chan );
void irc_chan_join ( WindowRec *window, gchar *name );
ChannelRec *irc_chan_new ( gchar *name );
void irc_chan_nickupd ( ChannelRec *chan );
void irc_chan_parsemode ( ChannelRec *chan, gchar *modestr );
void irc_chan_part ( ChannelRec *chan );
void irc_chan_part_all ( GList *channels );
void irc_chan_print ( ChannelRec *channel, TextLevel level, guchar *text, ... );
void irc_chan_remnick ( ChannelRec *chan, gchar *nick );
void irc_chan_select ( ChannelRec *chan );
void irc_chan_settopic ( ChannelRec *chan, gchar *topic );

ErrorCode irc_cmd_parse ( gchar *command, ChannelRec *channel );
gint irc_cmd_send ( ServerRec *server, char *cmd );

void irc_entry_parse ( ChannelRec *channel, gchar *string );

void irc_error_gen ( ErrorCode err );

NickRec *irc_nick_find_in_chan ( ChannelRec *chan, gchar *nick );

RootWindowRec *irc_root_new ( void );
void irc_root_print ( RootWindowRec *root, TextLevel level, guchar *text, ... );

void irc_server_connect ( ServerRec *server );
void irc_server_delete ( ServerRec *server );
void irc_server_disconnect ( ServerRec *server );
ServerRec *irc_server_find_by_name ( gchar *name );
ServerRec *irc_server_new ( WindowRec *window, gchar *host, gint port );
gint irc_server_parseline ( ServerRec *server, gchar *str );
void irc_server_print ( ServerRec *server, TextLevel level, guchar *text, ... );
gint irc_server_recvline ( ServerRec *server, gchar *str );

void irc_setup_apply ( void );
void irc_setup_open ( void );

/* Effectively a private function, use irc_window_print instead */
void irc_text_print ( WindowRec *win, TextLevel level, guchar *string );

void irc_window_addchan ( WindowRec *window, ChannelRec *chan );
WindowRec *irc_window_new ( void );
void irc_window_print ( WindowRec *win, TextLevel level, guchar *text, ... );
void irc_window_remchan ( ChannelRec *chan );
void irc_window_select ( WindowRec *window );


/* In misc.c */
gchar *get_param ( gchar **data );
gchar *get_params ( gchar *data, gint count, ...);
gint ischannel ( gchar chr );
gint isircflag ( gchar chr );
gint isop ( gchar chr );
gint isvoice ( gchar chr );
gint read_line ( gint socket, gint handle, gchar *str, gchar *buf, gint bufsize, gint *bufpos );

/* In net_nowait.c */
gint net_nowait_connect ( gchar *server, gint port, NetCallback func, gpointer data );
gint net_nowait_connect_new ( gchar *server, gint port, gint pipe );
gint net_nowait_connect_new_nopthread ( gchar *server, gint port, gint pipe );


/* In network.c */
gint net_accept ( gint handle, gchar *addr, gint *port );
gint net_connect ( gchar *server, gint port );
void net_disconnect ( gint fh );
gint net_getsockname ( gint handle, gchar *addr, gint *port );
gint net_listen ( gchar *ownaddr, gint *port );
gint net_receive ( gint fh, gchar *buf, gint len );
gint net_testconnect ( gint fh );
gint net_transmit ( gint fh, gchar *data, gint len );


/* Global Variables */

GList	*Windows, /* List of Windows */
	*RootWins,
	*Servers, /* List of Servers */
	*Channels; /* List of Channels */

gint	LastWinID,
	LastRootID,
	LastServID,
	LastChanID;

ChannelRec	*CurrentChannel;
RootWindowRec	*CurrentRoot;
ServerRec	*DefaultServer,
		*DummyServer;
WindowRec	*CurrentWindow;

SettingsRec	*GlobalSettings;
GdkFont		*NormalFont,
		*BoldFont,
		*ItalicFont;

extern	CommandRec		Commands[];
extern	CTCPRec			CTCPCmds[];
extern	EventCallback		*NumericEvents;
extern	EventEntryRec		NamedEvents[];
extern	SetupSectionRec		SetupSections[];
extern	gboolean		SetupOpen;
extern  gboolean                Exiting;

#ifdef GNOME
gint	sm_restarted;
gchar	*sm_justexit;
#endif

#endif
