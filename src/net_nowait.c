/*

    net_nowait.c : Threading for net_connect()

    Copyright (C) 1998 Marcus Brubaker

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.


    -- 18-Oct-98 (Michael Krause): added net_nowait_connect_new()
    which uses a pipe to report connection status, because the
    callback was not properly synchronized with the main thread in the
    old version. Additionally, the new code gives non-blocking
    connection buildup even without pthreads (only the DNS lookup is
    blocking).

*/

#include "config.h"

#include <stdio.h>
#include <glib.h>
#undef MAX
#undef MIN
#include <stdlib.h>
#include <string.h>

#include <netdb.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/signal.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>


#include "gnomeirc.h"

#ifdef PTHREADS

#include <pthread.h>

typedef struct {
    gchar *server;
    gint port;
    NetCallback func;
    gpointer data;
} NET_THREAD_REC;

typedef struct {
    gchar *server;
    gint port;
    gint pipe;
} NET_THREAD_NEW_REC;

#endif

gint net_nowait_connect_new_nopthread (	gchar	*server,
					gint	port,
					gint	pipe )
{
	struct sockaddr_in sin;
	struct hostent *hp;
	gint opt, fh, e;
	guint32 ip = 0;

	g_return_val_if_fail(server != NULL, 0);

	memset(&sin, 0, sizeof(sin));
	sin.sin_addr.s_addr = inet_addr(server); /* it's ip address? */

	if (sin.sin_addr.s_addr == -1) {
		/* it's host name? */
		hp = gethostbyname(server);
		if (hp == NULL)
			return 0;

		memcpy(&sin.sin_addr.s_addr, hp->h_addr, hp->h_length);
		if(hp->h_length == 4)
			memcpy(&ip, hp->h_addr, 4);
	}

	sin.sin_family = AF_INET;
	sin.sin_port = htons(port);

	fh = socket(AF_INET, SOCK_STREAM, 0);
	if(fh == -1)
		return 0;

	opt = 1;
	setsockopt(fh, SOL_SOCKET, SO_REUSEADDR, (char *) &opt, sizeof(opt));
	setsockopt(fh, SOL_SOCKET, SO_KEEPALIVE, (char *) &opt, sizeof(opt));
	fcntl(fh, F_SETFL, O_NONBLOCK);

	e = connect(fh, (struct sockaddr *) &sin, sizeof (struct sockaddr));

	if(e == 0 || errno == EINPROGRESS) {
		write(pipe, &fh, sizeof(fh));
		write(pipe, &ip, sizeof(ip));
		return 1;
	}
	else {
		perror("connect failed");
		close(fh);
		return 0;
	}
}

#ifdef PTHREADS

static gpointer net_callback( gpointer data )
{
	NET_THREAD_REC *rec;
	gint ret;

	rec = (NET_THREAD_REC *) data;
	ret = net_connect(rec->server, rec->port);

	rec->func(ret, rec->data);

	g_free(rec->server);
	g_free(rec);

	return NULL;
}

static gpointer net_nowait_connect_new_thread (gpointer data)
{
	NET_THREAD_NEW_REC *rec = (NET_THREAD_NEW_REC *) data;
	gint e;

	e = net_nowait_connect_new_nopthread(rec->server, rec->port, rec->pipe);

	if(e == 0) {
		e = -1;
		write(rec->pipe, &e, sizeof(e));
	}

	g_free(rec->server);
	g_free(rec);
	return NULL;
}

#endif

gint net_nowait_connect (	gchar		*server,
				gint		port,
				NetCallback	func,
				gpointer	data)
{
#ifdef PTHREADS
	NET_THREAD_REC *rec;
	pthread_t thread;

	g_return_val_if_fail(server != NULL, 0);
	g_return_val_if_fail(func != NULL, 0);

	rec = g_new(NET_THREAD_REC, 1);
	rec->server = g_strdup(server);
	rec->port = port;
	rec->func = func;
	rec->data = data;

	if (pthread_create (&thread, NULL, net_callback, rec)) {
		g_warning("net_nowait_connect() : pthread_create() failed!");
		func(net_connect(server, port), data);
		return 0;
	}
	return 1;
#else
	g_return_val_if_fail(server != NULL, 0);
	g_return_val_if_fail(func != NULL, 0);

	func(net_connect(server, port), data);
	return 1;
#endif
}

/*
  net_nowait_connect_new() returns:
     0 = can't connect
     1 = connection in progress

  Connection progress is reported through the given pipe:
     (int)     non-blocking file descriptor of the connection socket or -1
     (guint32) ip address of the server if file descriptor != -1

  When a valid file descriptor is returned, it should be select()ed for
  writing just like described in the connect() manpage.
*/

gint net_nowait_connect_new (	gchar	*server,
				gint	port,
				gint	pipe)
{
#ifdef PTHREADS
	NET_THREAD_NEW_REC *rec;
	pthread_t thread;

	g_return_val_if_fail(server != NULL, 0);

	rec = g_new(NET_THREAD_NEW_REC, 1);
	rec->server = g_strdup(server);
	rec->port = port;
	rec->pipe = pipe;

	if(pthread_create(&thread, NULL, net_nowait_connect_new_thread, rec)) {
		g_warning("net_nowait_connect_new(): pthread_create() failed!");
		g_free(rec->server);
		g_free(rec);
		return net_nowait_connect_new_nopthread(server, port, pipe);
	}
	else {
		pthread_detach(thread);
		return 1;
	}
#else
	return net_nowait_connect_new_nopthread(server, port, pipe);
#endif
}
