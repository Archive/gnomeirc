ServerRec *server_rec_new(char *host, int port, int sock, UserInfoRec *luser);
void server_rec_free(ServerRec *rec);
GList *find_server_by_id(gint id);
gint add_server_rec_to_list(ServerRec *rec);
int del_server_from_list(gint id);
int new_server_connection(char *host, int port, UserInfoRec *userinf);
void new_server_callback(int val, void *data);
void server_incoming_data(NetThreadRec *rec, gint sock, GdkInputCondition condition);
