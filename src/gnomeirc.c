#include "config.h"

#include "gnomeirc.h"

#include <glib.h>

#ifdef GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

gint main ( gint argc, gchar *argv[] )
{
	RootWindowRec *RootWin;
	gint errorcode;

	errorcode = irc_config_init ( );

	switch (errorcode) {
	case 0:
		break;
	case 1:
		g_warning ( "General failure reading config files, continuing anyway" );
		break;
	default:
		g_error ( "Unknown error reading config files, aborting" );
		return (1);
		break;
	}

	events_numeric_init ();

	if (DefaultServer == NULL)
		DefaultServer = DummyServer =
			irc_server_new ( CurrentWindow, " ", 1 );

	gui_init ( argc, argv );

	RootWin = gui_root_init ( );

	gtk_main ( );

	return (0);

}
