/*

 events-numeric.c : Functions to handle (numeric) IRC server replies

    Copyright (C) 1999 Marcus Brubaker

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <glib.h>

#include "gnomeirc.h"

static ChannelRec *names_chan; /* /NAMES */

EventCallback *NumericEvents;

/* Initialize events */
void events_numeric_init(void)
{
	names_chan = NULL;

	NumericEvents = g_new0 ( EventCallback, NUMEVENTS_MAX );

	NumericEvents[1] = (EventCallback) events_numeric_welcome;
	NumericEvents[4] = (EventCallback) events_numeric_connected;
	NumericEvents[303] = (EventCallback) events_numeric_ison;
	NumericEvents[305] = (EventCallback) events_numeric_unaway;
	NumericEvents[306] = (EventCallback) events_numeric_away;
	NumericEvents[311] = (EventCallback) events_numeric_whois;
	NumericEvents[312] = (EventCallback) events_numeric_whois_server;
	NumericEvents[313] = (EventCallback) events_numeric_whois_oper;
	NumericEvents[315] = (EventCallback) events_numeric_end_of_who;
	NumericEvents[317] = (EventCallback) events_numeric_whois_idle;
	NumericEvents[318] = (EventCallback) events_numeric_end_of_whois;
	NumericEvents[319] = (EventCallback) events_numeric_whois_channels;
//	NumericEvents[321] = (EventCallback) events_numeric_list_start;
//	NumericEvents[322] = (EventCallback) events_numeric_list;
//	NumericEvents[323] = (EventCallback) events_numeric_list_end;
	NumericEvents[324] = (EventCallback) events_numeric_channel_mode;
	NumericEvents[329] = (EventCallback) events_numeric_channel_created;
	NumericEvents[332] = (EventCallback) events_numeric_topic;
	NumericEvents[333] = (EventCallback) events_numeric_topic_info;
	NumericEvents[352] = (EventCallback) events_numeric_who;
	NumericEvents[353] = (EventCallback) events_numeric_names_list;
	NumericEvents[366] = (EventCallback) events_numeric_end_of_names;
	NumericEvents[367] = (EventCallback) events_numeric_ban_list;
	NumericEvents[368] = (EventCallback) events_numeric_end_of_banlist;
	NumericEvents[405] = (EventCallback) events_numeric_too_many_channels;
	NumericEvents[433] = (EventCallback) events_numeric_nick_in_use;
	NumericEvents[437] = (EventCallback) events_numeric_nick_unavailable;
	NumericEvents[471] = (EventCallback) events_numeric_channel_is_full;
	NumericEvents[473] = (EventCallback) events_numeric_invite_only;
	NumericEvents[474] = (EventCallback) events_numeric_banned;
	NumericEvents[475] = (EventCallback) events_numeric_bad_channel_key;
	NumericEvents[476] = (EventCallback) events_numeric_bad_channel_mask;
}

void events_deinit(void)
{
	g_free ( NumericEvents );
}

void events_numeric_welcome(EventRec *event)
{
	/* first welcome message found - we're connected now! */
	event->server->connected = 1;
	return;
}

void events_numeric_connected(EventRec *event)
{
	gchar *tempname;
	gchar *tptr;

	/* last welcome message found - commands can be sent to server now. */
	event->server->connected = 1;

	/* if on undernet, use undernet style wallops */
	tempname = g_strdup(event->server->host);
	for (tptr = tempname; *tptr != '\0'; tptr++)
		*tptr = tolower(*tptr);
	if (strstr(tempname, "undernet.org") != NULL)
		event->server->wallop_type = WALLOP_TYPE_UNET;
	else
		event->server->wallop_type = WALLOP_TYPE_STD;
	g_free(tempname);

//	gui_connected(eserver, 1);
}

void events_numeric_nick_in_use(EventRec *event)
{
	gchar *nick, *tmpstr;

	if (event->server->connected) {
		/* Already connected, no need to handle this anymore. */
		get_params(event->data, 2, NULL, &nick);
		irc_server_print(event->server, TLVL_SYSTEM,
			MSG_NICK_INUSE, nick);
		return;
	}

	/* nick already in use - need to change it .. */
	nick = event->server->user->nick;
	if (strlen(nick) < 9) {
		nick = g_new(char, strlen(nick)+2);
		sprintf(nick, "%s_", event->server->user->nick);
		g_free(event->server->user->nick);
		event->server->user->nick = nick;
	}
	else {
		gint n;

		/* keep adding numbers to end of nick */
		for (n = 8; n > 0; n--) {
			if (nick[n] < '0' || nick[n] > '9') {
				nick[n] = '1';
				break;
			}
        	   	else {
				if (nick[n] < '9') {
					nick[n]++;
					break;
				}
				else
					nick[n] = '0';
			}
		}
	}

	event->server->connected = 1;
	tmpstr = g_strdup_printf("NICK %s", nick);
	irc_cmd_send(event->server, tmpstr);
	g_free(tmpstr);

	event->server->connected = 0;
}

void events_numeric_names_list(EventRec *event)
{
	gchar *type, *channel, *names, *ptr;

	/* type = '=' = public, '*' = private, '@' = secret */
	get_params(event->data, 4, NULL, &type, &channel, &names);

	if (names_chan == NULL) {
		names_chan = irc_chan_find_in_serv(channel, event->server);
		if (names_chan == NULL) return;
		if (names_chan->Nicks != NULL) {

		  while (names_chan->Nicks->data) {
		    irc_chan_remnick(names_chan, names_chan->Nicks->data);
		  }

		  g_list_free(names_chan->Nicks);
		}
		names_chan->Nicks = NULL;
	}

	irc_chan_print ( names_chan, TLVL_SYSTEM,
		"%s names: %s", channel, names );

	while (*names != '\0') {
		while (*names == ' ') names++;
		ptr = names;
		while (*names != '\0' && *names != ' ') names++;
		if (*names != '\0') *names++ = '\0';

		irc_chan_addnick(names_chan, ptr);
	}
}

void events_numeric_end_of_names(EventRec *event)
{
	gchar *chan;

	get_params(event->data, 2, NULL, &chan);
	irc_chan_print ( irc_chan_find_by_name ( chan ), TLVL_SYSTEM,
		MSG_NAMES_END, chan );
	if (names_chan != NULL) irc_chan_nickupd(names_chan);
	names_chan = NULL;
}

void events_numeric_topic(EventRec *event)
{
	ChannelRec *chan;
	gchar *channame, *topic;

	get_params(event->data, 3, NULL, &channame, &topic);
	chan = irc_chan_find_in_serv(channame, event->server);
	if (chan == NULL) return;

	irc_chan_settopic ( chan, topic );
}

void events_numeric_topic_info(EventRec *event)
{
	ChannelRec *chan;
	time_t t;
	struct tm *tim;
	gchar *timestr, *channame, *topicby, *topictime;

	g_return_if_fail(event->data != NULL);

	get_params(event->data, 4, NULL, &channame, &topicby, &topictime);

	chan = irc_chan_find_in_serv( channame, event->server );
	if (chan == NULL) return;

	if (sscanf(topictime, "%lu", &t) != 1) t = 0;
	tim = localtime(&t);
	timestr = g_strdup(asctime(tim));
	if (timestr[strlen(timestr)-1] == '\n')
		timestr[strlen(timestr)-1] = '\0';

	irc_chan_print ( chan, TLVL_SYSTEM, MSG_TOPIC_SET,
		topicby, timestr );

	g_free(timestr);
}

void events_numeric_who(EventRec *event)
{
	gchar *nick, *channel, *user, *host, *status, *realname, *tmp;
	ChannelRec *chan;
	NickRec *rec;

	g_return_if_fail(event->data != NULL);

	get_params(event->data, 8, NULL, &channel, &user, &host, NULL, &nick, &status, &realname);

	/* skip hop count */
	while (*realname != '\0' && *realname != ' ') realname++;
	while (*realname == ' ') realname++;

	chan = irc_chan_find_in_serv ( channel, event->server);
	if (chan == NULL || (chan->queries & CHANQUERY_WHO) == 0) {
		tmp = g_strdup_printf(FRMT_WHO,
			channel, nick, status, user, host, realname);
		irc_chan_print ( chan, TLVL_SYSTEM, tmp );
		return;
	}

	rec = irc_nick_find_in_chan (chan, nick);
	if (rec == NULL) return;

	if (rec->host == NULL) rec->host = g_strdup(host);
}

void events_numeric_end_of_who(EventRec *event)
{
	ChannelRec *ch;
	gchar *chan;

	get_params(event->data, 2, NULL, &chan);
	ch = irc_chan_find_in_serv ( chan, event->server );

	if (ch != NULL && ch->queries & CHANQUERY_WHO) {
		/* End of /WHO query */
		ch->queries &= ~CHANQUERY_WHO;
	}
	else
		irc_chan_print ( ch, TLVL_SYSTEM, MSG_WHO_END );
}

void events_numeric_ban_list(EventRec *event)
{
	gchar *channame, *ban, *setby, *tims;
	glong secs, tim;
	ChannelRec *chan;

	get_params(event->data, 5, NULL, &channame, &ban, &setby, &tims);
	chan = irc_chan_find_in_serv ( channame, event->server );

	if (sscanf(tims, "%ld", &tim) != 1) tim = (glong) time(NULL);
	secs = (glong) time(NULL)-tim;

	if (chan == NULL || (chan->queries & CHANQUERY_BANLIST) == 0) {
		irc_chan_print ( chan, TLVL_SYSTEM,
			MSG_CHAN_BANSET,
			channame, ban, setby, secs );
		return;
	}

//	add_ban(chan, ban, setby, (time_t) tim);
}

void events_numeric_end_of_banlist(EventRec *event)
{
	ChannelRec *ch;
	gchar *chan;

	get_params(event->data, 2, NULL, &chan);
	ch = irc_chan_find_in_serv ( chan, event->server );

	if (ch != NULL && ch->queries & CHANQUERY_BANLIST)
		ch->queries &= ~CHANQUERY_BANLIST;
}

void events_numeric_ison(EventRec *event)
{
	gchar *online;
	GList *tmp, *next;

	get_params(event->data, 2, NULL, &online);

	if (event->server->ison_reqs <= 0) {
		/* user wrote /ISON.. */
		if (event->server->ison_reqs < 0) event->server->ison_reqs++;
		irc_server_print ( event->server,
			TLVL_SYSTEM, MSG_USER_ISON, online,
			event->server->host );
		return;
	}

	/* we're trying to find out who of the notify list members are in IRC */
	while (online != NULL && *online != '\0') {
		gchar *ptr;

		ptr = strchr(online, ' ');
		if (ptr != NULL) *ptr++ = '\0';
	        event->server->ison_tempusers =
			g_list_append(event->server->ison_tempusers,
				g_strdup(online));
		online = ptr;
	}

	if (--event->server->ison_reqs == 0) {
		/* all /ISON requests got - scan through notify list to check
			who's came and who's left */

		/* first check who's came to irc */
		GLIST_FOREACH(tmp, event->server->ison_tempusers) {
			GList *on;

			GLIST_FOREACH(on, event->server->ison_users) {
			if (g_strcasecmp((gchar *) tmp->data, (gchar *) on->data) == 0)
			break;
			}

			if (on == NULL) {
				/* not found from list, user's just joined to irc */
//				gui_notify_join(tmp->data);
				event->server->ison_users =
					g_list_append(event->server->ison_users,
						g_strdup(tmp->data));
			}
		}

		/* and then check who's left irc */
		GLIST_FOREACH(tmp, event->server->ison_users) {
			GList *on;

			next = tmp->next;
			GLIST_FOREACH(on, event->server->ison_tempusers) {
				if (g_strcasecmp((gchar *) tmp->data,
						 (gchar *) on->data) == 0)
					break;
			}
			if (on == NULL) {
				/* not found from list, user's left irc */
//				gui_notify_part(tmp->data);
				event->server->ison_users =
					g_list_remove(event->server->ison_users,
						tmp);
			}
		}

		/* free memory used by temp list */
		g_list_foreach(event->server->ison_tempusers, (GFunc) g_free, NULL);
		g_list_free(event->server->ison_tempusers);
		event->server->ison_tempusers = NULL;
	}
}

/* couldn't join to channel for some reason */
static void cannot_join(char *channel, char *reason)
{
	ChannelRec *chan;

	g_return_if_fail(channel != NULL);

	chan = irc_chan_find_by_name( channel );
	irc_chan_print ( chan, TLVL_SYSTEM, MSG_CHAN_NOJOIN,
		channel, reason );

	if (chan == NULL) return;

//	if (chan == chan->window->curchan)
//		irc_select_new_channel(chan->window);

	irc_chan_part(chan);
}

void events_numeric_nick_unavailable(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	if (*channel == '#' || *channel == '&') {
		/* channel is unavailable. */
		cannot_join(channel, CHAN_RSN_TEMP);
		return;
	}

	if (!event->server->connected) {
		/* nick is unavailable.. */
		events_numeric_nick_in_use(NULL);
	}

	irc_chan_print ( irc_chan_find_in_serv ( channel, event->server ),
		TLVL_SYSTEM, "Nick %s is unavailable", channel );
}

void events_numeric_too_many_channels(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_TMANY);
}

void events_numeric_channel_is_full(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_FULL);
}

void events_numeric_invite_only(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_INVITE);
}

void events_numeric_banned(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_BAN);
}

void events_numeric_bad_channel_key(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_BADKEY);
}

void events_numeric_bad_channel_mask(EventRec *event)
{
	gchar *channel;

	get_params(event->data, 2, NULL, &channel);
	cannot_join(channel, CHAN_RSN_BADMSK);
}

void events_numeric_channel_mode(EventRec *event)
{
	ChannelRec *chan;
	gchar *channame, *data;

	data = get_params(event->data, 2, NULL, &channame);
	chan = irc_chan_find_in_serv(channame, event->server);
	if (chan == NULL || (chan->queries & CHANQUERY_MODE) == 0) {
		irc_chan_print ( chan, TLVL_SYSTEM, "mode/%s [%s]",
			channame, data );
		return;
	}

	irc_chan_parsemode(chan, data);
	if ((chan->mode & CHANMODE_KEY) == 0 && chan->key != NULL) {
		g_free(chan->key);
		chan->key = NULL;
	}
	chan->queries &= ~CHANQUERY_MODE;
}	

void events_numeric_channel_created(EventRec *event)
{
	gchar *channel, *times, *timestr;
	time_t t;
	struct tm *tim;

	get_params(event->data, 3, NULL, &channel, &times);

	if (sscanf(times, "%ld", (long *) &t) != 1)
		tim = 0;
	tim = localtime(&t);
	timestr = g_strdup(asctime(tim));
	if (timestr[strlen(timestr)-1] == '\n')
		timestr[strlen(timestr)-1] = '\0';

	irc_chan_print ( irc_chan_find_in_serv ( channel, event->server ),
		TLVL_SYSTEM, MSG_CHAN_CREATE, channel, timestr );
	g_free(timestr);
}

void events_numeric_away(EventRec *event)
{
//    gui_set_away(1);
//    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_AWAY);
}

void events_numeric_unaway(EventRec *event)
{
//    gui_set_away(0);
//    drawtext(eserver, NULL, LEVEL_CRAP, IRCTXT_UNAWAY);
}

void events_numeric_whois(EventRec *event)
{
	gchar *nick, *user, *host, *realname;

	get_params(event->data, 6, NULL, &nick, &user, &host, NULL, &realname);
	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_FRMT, nick, user, host, realname );
}

void events_numeric_whois_idle(EventRec *event)
{
	gchar *nick, *secstr, *signon, *data;
	gint secs, h, m, s;
	time_t t;

	data = get_params(event->data, 3, NULL, &nick, &secstr);
	if (sscanf(secstr, "%d", &secs) == 0)
		secs = 0;
	t = 0;
	if (strstr(data, ", signon time") != NULL) {
		signon = get_param(&data);
		sscanf(signon, "%ld", (long *) &t);
	}

	h = secs/3600;
	m = (secs-h)/60;
	s = (secs-h)%60;

	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_IDLE, nick, h, m, s );
	if (t != 0) {
		gchar *timestr;
		struct tm *tim;

		tim = localtime(&t);
		timestr = g_strdup(asctime(tim));
		if (timestr[strlen(timestr)-1] == '\n')
			timestr[strlen(timestr)-1] = '\0';
		irc_server_print ( event->server, TLVL_SYSTEM,
			" (signon: %s)", timestr );
	}
}

void events_numeric_whois_server(EventRec *event)
{
	gchar *nick, *server, *desc;

	get_params(event->data, 4, NULL, &nick, &server, &desc);
	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_SRVR, nick, server, desc );
}

void events_numeric_whois_oper(EventRec *event)
{
	gchar *nick;

	get_params(event->data, 2, NULL, &nick);
	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_OPER, nick );
}

void events_numeric_whois_channels(EventRec *event)
{
	gchar *nick, *chans;

	get_params(event->data, 3, NULL, &nick, &chans);
	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_CHAN, nick, chans );
}

void events_numeric_end_of_whois(EventRec *event)
{
	gchar *nick;

	get_params(event->data, 2, NULL, &nick);
	irc_server_print ( event->server, TLVL_SYSTEM,
		MSG_WHOIS_END );
}
