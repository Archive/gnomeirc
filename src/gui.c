#include "config.h"

#include "gnomeirc.h"

#include <ctype.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#ifdef GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

static void gui_chancb_buttonclicked ( GtkWidget *widget, ChannelRec *channel )
{
	g_return_if_fail ( channel != NULL );
	irc_chan_select ( channel );
}

static void gui_entrycb_activate ( GtkWidget *w, WindowRec *window )
{
	gchar *text;

	if (window == NULL) return;

	text = gtk_entry_get_text ( GTK_ENTRY (w) );

	irc_entry_parse ( window->CurrentChannel, text );

	gtk_entry_set_text ( GTK_ENTRY (w), "" );
}

static void gui_menucb_about ( GtkWidget *widget, gpointer data )
{
#ifdef GNOME
	GtkWidget *about;
	gchar *authors[] ={ "Marcus Brubaker",
			    "Ben Pierce", NULL };
	about = gnome_about_new ( PACKAGE, VERSION,
				  "(C) 1998 Marcus Brubaker and Ben Pierce",
				  (const gchar **)authors, "", NULL );
	gtk_widget_show (about);
#else
#endif
}

static void gui_menu_callback ( GtkWidget *widget, gpointer data )
{
	irc_entry_parse ( CurrentChannel, (gchar *) data );
}

static void gui_notecb_select (	GtkWidget      *widget,
				gpointer       data )
{
	WindowRec *window;

	window = (WindowRec *) data;

	if (window->Root == CurrentRoot) CurrentWindow = window;

	window->Root->CurrentWindow = window;

	if (window->CurrentChannel != NULL)
		irc_chan_select ( window->CurrentChannel );
}

static void gui_rootcb_delete (	GtkWidget *widget,
				GdkEventAny *event,
				gpointer data )
{
	gui_root_close ( (RootWindowRec *) data );
}

static int gui_rootcb_keyevent (	GtkWidget *widget,
					GdkEventKey *event,
					RootWindowRec *window )
{
	g_return_val_if_fail ( window != NULL, 0 );

	gtk_widget_grab_focus ( window->CurrentWindow->GUIData->Entry );

	switch (event->keyval)
	{
		default:
			if (	(event->keyval == 'k' ||
				event->keyval == 'K') &&
				((event->state & GDK_MODIFIER_MASK) == 
				GDK_CONTROL_MASK) )
				gtk_entry_append_text ( GTK_ENTRY (window->CurrentWindow->GUIData->Entry), "\xBA" );
			return (0);
			break;
	}

	gtk_signal_emit_stop_by_name ( GTK_OBJECT (widget), "key_press_event" );
	return (1);
}

static void gui_rootcb_select (	GtkWidget *window,
				GdkEventFocus *event,
				gpointer data )
{
	g_return_if_fail ( data != NULL );

	CurrentRoot = (RootWindowRec *) data;

	CurrentWindow = CurrentRoot->CurrentWindow;

	gtk_widget_grab_focus ( CurrentWindow->GUIData->Entry );
}

#ifdef GNOME
GnomeUIInfo menu_Help [] = {
	GNOMEUIINFO_ITEM_STOCK ( "About GnomeIRC...", NULL,
		gui_menucb_about, GNOME_STOCK_MENU_ABOUT),
	GNOMEUIINFO_END
};

GnomeUIInfo menu_IRC [] = {
	{ GNOME_APP_UI_ITEM, "_Connect...", "Connect to IRC Server",
	  gui_menu_callback, "/connect", NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, 'C', GDK_CONTROL_MASK, NULL },
	{ GNOME_APP_UI_ITEM, "_Disconnect...", "Disconnect from an IRC Server",
	  gui_menu_callback, "/disconnect", NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, 'D', GDK_CONTROL_MASK, NULL },
	GNOMEUIINFO_SEPARATOR,
//	GNOMEUIINFO_MENU_PREFERENCES_ITEM (gui_menu_callback, "/setup"),
//	GNOMEUIINFO_SEPARATOR,
	GNOMEUIINFO_MENU_CLOSE_WINDOW_ITEM (gui_menu_callback, "/close root"),
	GNOMEUIINFO_MENU_EXIT_ITEM (gui_menu_callback, "/quit"),
	GNOMEUIINFO_END
};

GnomeUIInfo menu_Window [] = {
	{ GNOME_APP_UI_ITEM, "_New Toplevel Window", "Create a New Toplevel Window",
	  gui_menu_callback, "/window new root", NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, GNOME_KEY_NAME_NEW, GNOME_KEY_MOD_NEW, NULL },
	{ GNOME_APP_UI_ITEM, "New Window_page", "Create a New Windowpage",
	  gui_menu_callback, "/window new page", NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, 'P', GDK_CONTROL_MASK, NULL },
	GNOMEUIINFO_SEPARATOR,
	{ GNOME_APP_UI_ITEM, "Close Current Windowpage", "Close Current Windowpage",
	  gui_menu_callback, "/close page", NULL, GNOME_APP_PIXMAP_NONE,
	  NULL, 0, 0, NULL },
	GNOMEUIINFO_END
};

GnomeUIInfo menu_MAIN [] = {
	{ GNOME_APP_UI_SUBTREE, "_IRC", "IRC Menu", menu_IRC, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL, 'I', GDK_MOD1_MASK, NULL },
	{ GNOME_APP_UI_SUBTREE, "_Window", "Window Menu", menu_Window, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL, 'W', GDK_MOD1_MASK, NULL },
	{ GNOME_APP_UI_SUBTREE, "_Help", "Help Menu", menu_Help, NULL, NULL,
	  GNOME_APP_PIXMAP_NONE, NULL, 'H', GDK_MOD1_MASK, NULL },
	GNOMEUIINFO_END
};
#endif

GdkColor colors [] = {
	{ 0, 0, 0, 0 }, /* black 0 */
	{ 0, 0, 0, 0x7fff }, /* blue 1 */
	{ 0, 0, 0x7fff, 0 }, /* green 2 */
	{ 0, 0, 0x7fff, 0x7fff }, /* cyan 3 */
	{ 0, 0x7fff, 0, 0 }, /* red 4 */
	{ 0, 0x7fff, 0, 0x7fff }, /* magenta 5 */
	{ 0, 0x7fff, 0x7fff, 0 }, /* yellow 6 */
	{ 0, 0xbfff, 0xbfff, 0xbfff }, /* white 7 */
	{ 0, 0x3fff, 0x3fff, 0x3fff }, /* light black 8 */
	{ 0, 0, 0, 0xffff }, /* bright blue 9 */
	{ 0, 0, 0xffff, 0 }, /* bright green 10 */
	{ 0, 0, 0xffff, 0xffff }, /* bright cyan 11 */
	{ 0, 0xffff, 0, 0 }, /* bright red 12 */
	{ 0, 0xffff, 0, 0xffff }, /* bright magenta 13 */
	{ 0, 0xffff, 0xffff, 0 }, /* bright yellow 14 */
	{ 0, 0xffff, 0xffff, 0xffff }, /* bright white 15 */
};

void gui_exit ( void )
{
  g_list_free(Servers);
  g_list_free(RootWins);
  g_list_free(Channels);
  g_list_free(Windows);
  
        irc_config_deinit();

	gtk_main_quit ( );
}

void gui_init ( gint argc, gchar *argv[] )
{

#ifdef GNOME
	struct poptOption prog_options[] = {
		{ "discard-session", 'd', POPT_ARG_STRING, &sm_justexit, 0,
		  "Discard session", "ID" },
		{ NULL, '\0', 0, NULL, 0 }
	};

	gnome_init_with_popt_table ( PACKAGE, VERSION, argc, argv,
		prog_options, 0, NULL );
#else
	gtk_init ( &argc, &argv );
#endif

	gtk_rc_parse ( "~/.gnomeirc/gtkrc" );

	NormalFont = gdk_font_load ( GlobalSettings->fname_normal );
	BoldFont = gdk_font_load ( GlobalSettings->fname_bold );
	ItalicFont = gdk_font_load ( GlobalSettings->fname_italic );

}

void gui_chan_deselect ( ChannelGUIData *GUIData )
{
	g_return_if_fail ( GUIData != NULL );
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (GUIData->Button), FALSE );
	if (GUIData->Nickscroll) gtk_widget_hide(GUIData->Nickscroll);
}

gboolean gui_chan_is_selected ( ChannelGUIData *GUIData )
{
	g_return_val_if_fail ( (GUIData != NULL) &&
		(GTK_IS_TOGGLE_BUTTON (GUIData->Button) ), (-1) );
	return ( GTK_TOGGLE_BUTTON (GUIData->Button)->active );
}

ChannelGUIData *gui_chan_new ( ChannelRec *chan )
{
	ChannelGUIData *GUIData;

	g_return_val_if_fail ( chan != NULL, NULL );

	GUIData = g_new0 ( ChannelGUIData, 1 );
	GUIData->Nicklist = NULL;
	GUIData->Button = gtk_toggle_button_new_with_label ( chan->Name );

	gtk_signal_connect ( GTK_OBJECT (GUIData->Button), "released",
		GTK_SIGNAL_FUNC (gui_chancb_buttonclicked), chan );

	gtk_widget_show ( GUIData->Button );

	return (GUIData);
}

void gui_chan_select ( ChannelRec *chan )
{
	g_return_if_fail ( chan != NULL );
	gtk_toggle_button_set_active ( GTK_TOGGLE_BUTTON (chan->GUIData->Button), TRUE );
	if (chan->GUIData->Nickscroll) gtk_widget_show(chan->GUIData->Nickscroll);
}

void gui_chan_settopic ( ChannelRec *chan )
{
	gchar *title;

	if ( chan == NULL ) return;

	title = g_strconcat ( chan->Name, ": ", chan->Topic );

	gui_root_settitle ( chan->Window->Root->GUIData, title );

	g_free ( title );
}

void nickupd_insert ( NickRec *nick, GtkCList *list )
{
	if (nick == NULL) return;

	gtk_clist_append ( list, &(nick->nick) );
}

void gui_chan_nickupd ( ChannelRec *channel )
{
  GtkPaned *pane;

	g_return_if_fail ( channel != NULL && channel->GUIData != NULL );

	pane = GTK_PANED(channel->Window->GUIData->MainPane);
		
	if ( channel->GUIData->Nicklist == NULL ) {
		channel->GUIData->Nicklist = gtk_clist_new ( 1 );
		channel->GUIData->Nickscroll =
			gtk_scrolled_window_new ( NULL, NULL );
		gtk_scrolled_window_set_policy (
			GTK_SCROLLED_WINDOW ( channel->GUIData->Nickscroll ),
			GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC );
		gtk_clist_set_column_auto_resize (
			GTK_CLIST (channel->GUIData->Nicklist), 0, TRUE );
		gtk_clist_set_column_min_width (
			GTK_CLIST (channel->GUIData->Nicklist), 0, 15 );
		gtk_clist_set_column_max_width (
			GTK_CLIST (channel->GUIData->Nicklist), 0, -1 );
		gtk_container_add (
			GTK_CONTAINER (channel->GUIData->Nickscroll),
			channel->GUIData->Nicklist );
		
		gtk_paned_add2(pane, channel->Window->GUIData->Nickbox);
		gtk_box_pack_end ( GTK_BOX (channel->Window->GUIData->Nickbox),
			channel->GUIData->Nickscroll, TRUE, TRUE, 1 );

		gtk_widget_show_all ( channel->Window->GUIData->Nickbox );
	}
	else
		gtk_clist_clear ( GTK_CLIST (channel->GUIData->Nicklist) );

	gtk_paned_set_position(pane, pane->max_position - DEFAULT_NICKLIST_SIZE);

	gtk_clist_freeze ( GTK_CLIST (channel->GUIData->Nicklist) ); 

	g_list_foreach ( channel->Nicks, (GFunc)nickupd_insert,
		channel->GUIData->Nicklist );

	gtk_clist_set_column_width (  GTK_CLIST (channel->GUIData->Nicklist),
		0, gtk_clist_optimal_column_width (
			GTK_CLIST (channel->GUIData->Nicklist), 0 ) );

	gtk_clist_thaw ( GTK_CLIST (channel->GUIData->Nicklist) );
}

void gui_chan_part ( ChannelRec *chan )
{
  GtkPaned *pane;

	if ( chan == NULL ) return;

	gtk_widget_destroy ( chan->GUIData->Button );
	if ( chan->GUIData->Nickscroll != NULL )
		gtk_widget_destroy ( chan->GUIData->Nickscroll );

	pane = GTK_PANED(chan->Window->GUIData->MainPane);
	gtk_paned_set_position(pane, pane->max_position);
}

void gui_root_addwindow ( RootWindowRec *rootwin, WindowRec *window )
{
	g_return_if_fail ( rootwin != NULL );
	g_return_if_fail ( window != NULL );
	g_return_if_fail ( rootwin->GUIData != NULL );
	g_return_if_fail ( window->GUIData != NULL );

	window->Root = rootwin;

	gtk_notebook_append_page ( GTK_NOTEBOOK (rootwin->GUIData->Notebook),
		window->GUIData->Box, window->GUIData->Label );

	gtk_signal_connect ( GTK_OBJECT (window->GUIData->Box), "map",
		gui_notecb_select, window );

	rootwin->CurrentWindow = window;

	if (rootwin->WinList == NULL) rootwin->WinList = g_list_alloc ( );

	rootwin->WinList = g_list_append ( rootwin->WinList, (gpointer) window );

	gtk_widget_show_all ( rootwin->GUIData->Notebook );

	irc_window_select ( window );
}

static void win_massclose ( WindowRec *window, gpointer nothing )
{
	gint pagenum;

	if (window == NULL) return;

	pagenum = gtk_notebook_page_num ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		window->GUIData->Box );

	gtk_widget_destroy ( window->GUIData->Entry );

	gtk_notebook_remove_page ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		pagenum );

	irc_chan_part_all ( window->Channels );

	Windows = g_list_remove ( Windows, window );

	g_free ( window->GUIData );
	g_free ( window );

}

void gui_root_close ( RootWindowRec *rootwin )
{
	g_return_if_fail ( rootwin != NULL );
	g_return_if_fail ( rootwin->GUIData != NULL );

	if ( rootwin->WinList != NULL ) {
		g_list_foreach ( rootwin->WinList, (GFunc)win_massclose, NULL );
		g_list_free ( rootwin->WinList );
	}

	gui_window_renum ( );

	RootWins = g_list_remove ( RootWins, rootwin );
	if ( g_list_length ( RootWins ) <= 1 && !Exiting ) gui_exit ( );
	else gtk_widget_destroy ( rootwin->GUIData->app );

	gui_root_renum ( );

	g_free ( rootwin->title );
	g_free ( rootwin->GUIData );
	g_free ( rootwin );
}

RootWindowRec *gui_root_init ( )
{
	RootWindowRec *rootwin;
	WindowRec *window;

	rootwin = irc_root_new ( );

	window = irc_window_new ( );

	gui_root_addwindow ( rootwin, window );

	gtk_widget_show_all ( rootwin->GUIData->app );

	CurrentRoot = rootwin;
	CurrentWindow = window;

	return (rootwin);
}

void gui_root_create ( void )
{
	RootWindowRec *rootwin;
	WindowRec *window;

	rootwin = irc_root_new ( );
	window = irc_window_new ( );
	gui_root_addwindow ( rootwin, window );

	gtk_widget_show_all ( rootwin->GUIData->app );
}

RootWindowGUIData *gui_root_new ( RootWindowRec *rootwin )
{
	GtkWidget/*	*toolbar, Make this completely configurable via config files */
			*statusbar;

	g_return_val_if_fail ( rootwin != NULL, NULL );

	rootwin->GUIData = g_new0 ( RootWindowGUIData, 1 );

#ifdef GNOME
	rootwin->GUIData->app = gnome_app_new ( PACKAGE, rootwin->title );

	gtk_window_set_policy ( GTK_WINDOW (rootwin->GUIData->app),
		TRUE, TRUE, FALSE );

	gtk_widget_set_usize(GTK_WIDGET(rootwin->GUIData->app), 500, 300);

	gtk_signal_connect ( GTK_OBJECT (rootwin->GUIData->app), "focus_in_event",
		gui_rootcb_select, rootwin );
	gtk_signal_connect ( GTK_OBJECT (rootwin->GUIData->app), "delete_event",
		gui_rootcb_delete, rootwin );
	gtk_signal_connect ( GTK_OBJECT (rootwin->GUIData->app), "key_press_event",
		GTK_SIGNAL_FUNC (gui_rootcb_keyevent), rootwin );

	statusbar = gtk_statusbar_new ( );
	gtk_statusbar_push ( GTK_STATUSBAR (statusbar), 1, "[Not Connected]" );

	gnome_app_create_menus ( GNOME_APP (rootwin->GUIData->app), menu_MAIN );

	rootwin->GUIData->Notebook = gtk_notebook_new ( );
	gtk_notebook_set_scrollable ( GTK_NOTEBOOK (rootwin->GUIData->Notebook), TRUE);

	gnome_app_set_contents ( GNOME_APP (rootwin->GUIData->app),
		rootwin->GUIData->Notebook );

	gnome_app_set_statusbar ( GNOME_APP (rootwin->GUIData->app), statusbar );
#endif

	return (rootwin->GUIData);

}

void gui_root_renum ( void )
{
}

void gui_root_settitle ( RootWindowGUIData *GUIData, gchar *title )
{
	g_return_if_fail ( GUIData != NULL );

	gtk_window_set_title ( GTK_WINDOW(GUIData->app), title );
}

void gui_setup_open ( )
{
	GtkWidget *propbox;
	SetupBuildRec *setuppage;
	SetupSectionRec section;
	gint index = 0;

#ifdef GNOME
	propbox = gnome_property_box_new ( );

	section = SetupSections[index];

	while (section.name != NULL) {
		setuppage = section.initfunc( section.name, SETUP_STYLE_PAGE );
		gnome_property_box_append_page ( GNOME_PROPERTY_BOX (propbox),
			setuppage->page,
			setuppage->label );
		section = SetupSections[++index];
	}

	gtk_widget_show_all ( propbox );
#endif
}

void gui_text_print ( guchar *string )
{
	gui_text_winprint ( CurrentWindow, string );
}

void gui_text_printf ( guchar *format, ...)
{
	guchar *string;
	va_list ap;

	string = g_new0 ( guchar, 1000 );

	va_start ( ap, format );

	(void) vsnprintf ( string, 999, format, ap );
	gui_text_print ( string );

	va_end ( ap );

	g_free ( string );
}

gboolean gui_text_sbdown ( WindowRec *window )
{
	GtkAdjustment *adj;

	g_return_val_if_fail(window != NULL, 1);
	g_return_val_if_fail(window->GUIData != NULL, 1);

	adj = GTK_TEXT (window->GUIData->Text)->vadj;

	if (adj->value >= (adj->upper - adj->page_size)) return TRUE;

	return FALSE;
}

void gui_text_setsbdown ( WindowRec *window )
{
	GtkAdjustment *adj;

	g_return_if_fail(window != NULL);
	g_return_if_fail(window->GUIData != NULL);

	adj = GTK_TEXT (window->GUIData->Text)->vadj;
	gtk_adjustment_set_value(adj, adj->upper - adj->lower - adj->page_size);
}

void gui_text_winprint ( WindowRec *window, guchar *string )
{
	guchar	 	*str,
			*pos,
			*temp,
			*ptemp;
	gboolean	sbdown;
	GdkFont		*font = NormalFont;
	GdkColor	*bg = GlobalSettings->BGColor,
			*fg = GlobalSettings->FGColor;

	g_return_if_fail ( window != NULL && window->GUIData != NULL );

	if ( string[strlen(string) - 1] != '\n' )
		pos = str = g_strdup_printf ( "%s\n", string );
	else 
		pos = str = g_strdup ( string );

	sbdown = gui_text_sbdown ( window );

	gtk_text_freeze ( GTK_TEXT (window->GUIData->Text) );

	if ( strchr (str, 186) == NULL ) {
		gtk_text_insert ( GTK_TEXT (window->GUIData->Text), font,
			fg, bg, str, -1 );
		gtk_text_thaw ( GTK_TEXT (window->GUIData->Text) );
		g_free ( str );
		return;
	}

	if (*str != 186) {
		temp = strchr ( str, 186 );
		gtk_text_insert ( GTK_TEXT (window->GUIData->Text), font, fg,
			bg, str, (temp - str) );
	}
	

	for ( temp = strchr(str,186); temp != NULL; temp = strchr(pos,186) ) {

		temp++;

		switch (*temp)
		{
			case (186):
				font = NormalFont;
				bg = GlobalSettings->BGColor;
				fg = GlobalSettings->FGColor;
				temp++;
				break;
			case ('!'):
				font = BoldFont;
				temp++;
				break;
			case ('@'):
				font = ItalicFont;
				temp++;
				break;
			case ('('):
				pos = strchr ( temp, ')' );
				if ( pos != NULL ) {
					gint colornum;
					gchar *numtmp, *partmp;
					temp++;
					*pos = '\0';
					numtmp = g_strdup ( temp );
					*pos = ')';
					temp = ++pos;
					colornum = strtol ( numtmp,
							(char **)&partmp, 10 );
					if ( (colornum <= 15) &&
					     (colornum >= 0) )
						fg = &colors [ colornum ];
					numtmp = strchr ( numtmp, ',' );
					if ( numtmp != NULL ) {
						numtmp++;
						colornum = strtol ( numtmp,
							(char **)&partmp, 10 );
						if ( (colornum <= 15) &&
						     (colornum >= 0) )
							bg = &colors[colornum];
					}
				}
				break;
		}

		ptemp = strchr ( temp, 186 );

		gtk_text_insert ( GTK_TEXT (window->GUIData->Text), font, fg,
			bg, temp, ptemp != NULL?(ptemp - temp):(-1) );

		pos = temp;
	}

	gtk_text_thaw ( GTK_TEXT (window->GUIData->Text) );

	if (sbdown) gui_text_setsbdown ( window );

	g_free(str);
}

void gui_text_winprintf ( WindowRec *window, guchar *format, ...)
{
	guchar *string;
	va_list ap;

	string = g_new0 ( guchar, 1000 );

	va_start ( ap, format );

	(void) vsnprintf ( string, 999, format, ap );
	gui_text_winprint ( window, string );

	va_end ( ap );

	g_free ( string );
}

void gui_window_addchan (	WindowGUIData *winguidata,
				ChannelGUIData *changuidata )
{
	g_return_if_fail ( winguidata != NULL );
	g_return_if_fail ( changuidata != NULL );

	gtk_box_pack_start ( GTK_BOX (winguidata->BtnBox),
		changuidata->Button, FALSE, FALSE, 0 );
}

void gui_window_close ( WindowRec *window )
{
	gint pagenum;

	g_return_if_fail ( window != NULL );
	g_return_if_fail ( window->GUIData != NULL );
	g_return_if_fail ( window->Root != NULL );
	g_return_if_fail ( window->Root->GUIData != NULL );

	window->Root->WinList = g_list_remove ( window->Root->WinList, window );

	irc_chan_part_all ( window->Channels );
	g_list_free ( window->Channels );

	pagenum = gtk_notebook_page_num ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		window->GUIData->Box );

	gtk_notebook_remove_page ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		pagenum );

	gtk_widget_destroy ( window->GUIData->Entry );

	Windows = g_list_remove ( Windows, window );

	gui_window_renum ( );

	g_free ( window->GUIData );
	g_free ( window );
}

void win_tabrelabel ( ChannelRec *channel, gchar *label )
{
	if (label == NULL) return;
	if (channel == NULL) return;

	if ( label[strlen(label) - 1] != '[' ) sprintf ( label, "%s,%s", label, channel->Name );
	else sprintf ( label, "%s%s", label, channel->Name );
}

void gui_window_labelupd ( WindowRec *window )
{
	gchar *newlabel = NULL;

	g_return_if_fail ( window != NULL );
	g_return_if_fail ( window->Root != NULL );
	g_return_if_fail ( window->Root->GUIData != NULL );

	if ( newlabel == NULL ) {
		newlabel = g_new0 ( gchar, 500 );
		snprintf ( newlabel, 499, "%d: [", window->WinIDNum );
		g_list_foreach ( window->Channels, (GFunc) win_tabrelabel, newlabel );
		newlabel = strcat ( newlabel, "]" );
	}

	gtk_notebook_set_tab_label_text ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		window->GUIData->Box, newlabel );
}

WindowGUIData *gui_window_new ( WindowRec *window )
{
	gchar *labeltext;

	g_return_val_if_fail ( window != NULL, NULL );

	labeltext = g_strdup_printf ("%d: []", window->WinIDNum);

	window->GUIData = g_new0 ( WindowGUIData, 1 );

	window->GUIData->Label = gtk_label_new ( labeltext ); 

	window->GUIData->Box = gtk_vbox_new ( FALSE, 0 );

	window->GUIData->BtnBox = gtk_hbox_new ( FALSE, 1 );

	window->GUIData->Textbox = gtk_hbox_new ( FALSE, 0 );

	window->GUIData->MainPane = gtk_hpaned_new ( );
	
	window->GUIData->Nickbox = gtk_hbox_new ( FALSE, 0 );

	window->GUIData->Text = gtk_text_new ( NULL, NULL );
	gtk_text_set_editable ( GTK_TEXT (window->GUIData->Text), FALSE );

	window->GUIData->Scrollbar = gtk_vscrollbar_new ( GTK_TEXT (window->GUIData->Text)->vadj );

	window->GUIData->Entry = gtk_entry_new_with_max_length ( 500 );
	gtk_widget_grab_focus ( window->GUIData->Entry );
	gtk_signal_connect ( GTK_OBJECT (window->GUIData->Entry), "activate",
		(GtkSignalFunc) gui_entrycb_activate, window );

	gtk_box_pack_start ( GTK_BOX (window->GUIData->Textbox),
		window->GUIData->Text, TRUE, TRUE, 0 );

	gtk_box_pack_start ( GTK_BOX (window->GUIData->Textbox),
		window->GUIData->Scrollbar, FALSE, FALSE, 0 );

	gtk_box_pack_start ( GTK_BOX (window->GUIData->Box),
		window->GUIData->BtnBox, FALSE, FALSE, 0 );

	gtk_paned_add1(GTK_PANED(window->GUIData->MainPane), window->GUIData->Textbox);

	gtk_box_pack_start ( GTK_BOX (window->GUIData->Box),
		window->GUIData->MainPane, TRUE, TRUE, 0 );

	gtk_box_pack_end ( GTK_BOX (window->GUIData->Box),
		window->GUIData->Entry, FALSE, FALSE, 0 );

	g_free(labeltext);

	return (window->GUIData);
}

static void win_reassign ( gpointer window, gpointer nothing )
{

	if (window == NULL) return;

	((WindowRec *)window)->WinIDNum = LastWinID++;
}

static void win_rename ( gpointer window, gpointer nothing )
{
	if (window == NULL) return;

	gui_window_labelupd ( (WindowRec *) window );
}

void gui_window_renum ( void )
{
	LastWinID = 0;

	g_list_foreach ( Windows, win_reassign, NULL );

	g_list_foreach ( Windows, win_rename, NULL );
}

void gui_window_select ( WindowRec *window )
{
	int pagenum;

	g_return_if_fail ( window != NULL );
	g_return_if_fail ( (window->Root != NULL) &&
			   (window->Root->GUIData != NULL) );

	pagenum = gtk_notebook_page_num ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		window->GUIData->Box );

	gtk_notebook_set_page ( GTK_NOTEBOOK (window->Root->GUIData->Notebook),
		pagenum );
}
