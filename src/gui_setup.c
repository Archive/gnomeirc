#include "config.h"

#include "gnomeirc.h"

#include <glib.h>

#ifdef GNOME
#include <gnome.h>
#else
#include <gtk/gtk.h>
#endif

SetupSectionRec SetupSections[] = {
	{ "Servers", (SetupInitFunc) setup_server },
	{ NULL, NULL }
};

SetupBuildRec *setup_server ( gchar *name, SetupStyle style )
{
	GtkWidget *toggle;
	SetupBuildRec *spage = g_new0 ( SetupBuildRec, 1 );

	switch (style) {
		case SETUP_STYLE_PAGE:
			toggle = gtk_toggle_button_new_with_label ( "foo" );
			spage->page = gtk_hbox_new ( FALSE, 1 );
			gtk_box_pack_start ( GTK_BOX (spage->page), toggle,
				FALSE, FALSE, 1 );
			spage->label = gtk_label_new ( name );
			break;
		case SETUP_STYLE_MENU:
			break;
		case SETUP_STYLE_BOTH:
			break;
	}

	return (spage);
}
