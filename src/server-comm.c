#include "network.h"
#include "server-comm.h"

/* make a new UserInfoRec.  use userinfo_rec_free to delete */
UserInfoRec *userinfo_rec_new(char *nick, char *real, char *user, char *host)
{
  UserInfoRec *newrec = g_new0(UserInfoRec, 1);

  if (nick) newrec->nick = g_strdup(nick);
  if (real) newrec->realname = g_strdup(real);
  if (user) newrec->username = g_strdup(user);
  if (host) newrec->host = g_strdup(host);

  return(newrec);
}

/* delete a UserInfoRec */
void userinfo_rec_free(UserInfoRec *rec)
{
  if (!rec) return;

  if (rec->nick) g_free(rec->nick);
  if (rec->realname) g_free(rec->realname);
  if (rec->username) g_free(rec->username);
  if (rec->host) g_free(rec->host);

  g_free(rec);
}

/* allocate a new ServerRec.  creates a duplicate UserInfoRec *
 * for its own use.  use server_rec_free to free              */
ServerRec *server_rec_new(char *host, int port, int sock, UserInfoRec *luser)
{
  ServerRec *newrec = g_new0(ServerRec, 1);
  UserInfoRec *newluser;

  if (host) newrec->host = g_strdup(host);
  newrec->port = port;
  newrec->socket = sock;
  if (luser) {
    newluser = userinfo_rec_new(luser->nick, luser->realname, luser->username, luser->host);
    newrec->user = newluser;
  }

  return(newrec);
}

/* delete that damn ServerRec */
void server_rec_free(ServerRec *rec)
{
  if (!rec) return;

  if (rec->host) g_free(rec->host);
  if (rec->user) userinfo_rec_free(rec->user);

  g_free(rec);
}

/* get the glist item of a ServerRec by its IDnum */
GList *find_server_by_id(gint id)
{
  GList *tmplist;
  ServerRec *tmprec;

  /* if bad param or server list is empty, return NULL */
  if (!id || !Servers) return NULL;

  tmplist = tmprec = NULL;

  GLIST_FOREACH(tmplist, Servers)
    {
      tmprec = (ServerRec *)tmplist->data;

      /* we got it! stop the loop */
      if (tmprec->ServIDNum == id) break;

      /* couldnt find it...  null the tmp-vars */
      if (!tmplist->next) {
	tmplist = NULL;
	tmprec = NULL;
	break;
      }
    }

  return(tmplist);
}

/* adds a ServerRec to the global list and returns the server id # */
gint add_server_rec_to_list(ServerRec *rec)
{
  GList *tmplist;
  ServerRec *tmprec;
  gint tmpid;

  if (!rec) return(0);

  if (!Servers) Servers = g_list_alloc();
  tmplist = tmprec = NULL;

  /* find the first open ID# and use it (modify the rec to match too) */
  for(tmpid = 0; !find_server_by_id(tmpid); tmpid++)
    {
      if (!find_server_by_id(tmpid)) {
	g_list_append(Servers, (gpointer)rec);
	rec->ServIDNum = tmpid;
      }
    }

  return(tmpid);
}

/* remove server rec from global list and free it  *
 * returns -1 if error, 0 if ID not found, 1 if ok */
int del_server_from_list(gint id)
{
  GList *tmplist;
  ServerRec *tmprec;

  /* server list empty */
  if (!Servers) return(-1);

  for(tmplist = Servers; tmplist->next; tmplist = tmplist->next)
    {
      tmprec = (ServerRec *)tmplist->data;
      
      /* got it! free it, remove it and return success */
      if (tmprec->ServIDNum == id) {
	g_list_remove(tmplist, (gpointer)tmprec);
	server_rec_free(tmprec);
	return(1);
      }
    }

  return(0);
}

/* connect to a new server */
int new_server_connection(char *host, int port, UserInfoRec *userinf)
{
  ServerRec *newserver;

  if (!host || !port) return(-1);

  if (!userinf) userinf = GlobalSettings->userinfo;

  newserver = new_server_rec(host, port, 0, userinf);

  /* connect failed, free the ServerRec and return 0 */
  if (!(net_connect(host, port, (NetCallbackFunc)new_server_callback, (gpointer)newserver))) {
    server_rec_free(newserver);
    return(FALSE);
  }

  return(TRUE);
}

/* called upon successful connection of new server */
void new_server_callback(int val, NetThreadRec *srec)
{
  ServerRec *theserv;

  if (val < 0) {
    /* error, do somethin! */
    return;
  }

  /* take the ServerRec outta the NetThreadRec and drop the NetThreadRec */
  theserv = (ServerRec *)srec->data;
  netthread_rec_free(srec);
  /* dont forget to record the sock id in the ServerRec
  theserv->sock = val;

  /* set up the incoming data caller, passing it the server ID returned *
   * when adding the server to the connected server list                */
  gdk_input_add_full(val, GDK_INPUT_READ, (GdkInputFunction)server_incoming_data, (gpointer)add_server_to_list(theserv), NULL);
}

void server_incoming_data(gint servID, gint sock, GdkInputCondition condition)
{
  /* if it aint incoming data we dont givadogabone */
  if (condition != GDK_INPUT_READ) return;

  /* UNFINISHED */

}
