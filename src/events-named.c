/*

 events-named.c : Functions to handle (named) IRC server replies

    Copyright (C) 1999 Marcus Brubaker

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include "config.h"
#include <string.h>
#include <ctype.h>

#include "gnomeirc.h"

EventEntryRec NamedEvents[] =
{
	{ "PRIVMSG", (EventCallback) events_named_privmsg },
	{ "NOTICE", (EventCallback) events_named_notice },
	{ "NICK", (EventCallback) events_named_nick },
	{ "QUIT", (EventCallback) events_named_quit },
	{ "JOIN", (EventCallback) events_named_join },
	{ "PART", (EventCallback) events_named_part },
	{ "PING", (EventCallback) events_named_ping },
	{ "PONG", (EventCallback) events_named_pong },
	{ "MODE", (EventCallback) events_named_mode },
	{ "KICK", (EventCallback) events_named_kick },
	{ "INVITE", (EventCallback) events_named_invite },
	{ "TOPIC", (EventCallback) events_named_new_topic },
	{ "WALLOPS", (EventCallback) events_named_wallops },
	{ "ERROR", (EventCallback) events_named_error },
	{ NULL, NULL }
};

void events_named_privmsg( EventRec *event )
{
	gchar		*channame,
			*data,
			format[2048];
	gint		level;
	ChannelRec	*chan;
	CTCPEventRec	*ctcpevent;

	g_return_if_fail ( event != NULL );

	data = g_strdup ( event->data );

	channame = get_param(&data); /* Channel or nick name */
	if (*data == ':') data++;

	if (event->nick == NULL) event->nick = "!server!";

	if (*data == 1) {
		/* ctcp message */
		gchar *ptr;
		gint n, ret;

//		if (ignore_check(event->server, event->nick, LEVEL_CTCP))
//			return;
//		flood_newmsg(event->server, LEVEL_CTCP | is_channel(*channame) ? LEVEL_PUBLIC : LEVEL_MSGS, event->nick);

		/* remove the later \001 */
		ptr = strchr(++data, 1);
		if (ptr != NULL)
			*ptr = *(ptr+1) == '\0' ? '\0' : ' ';

		/* get ctcp command */
		for (ptr = data; *ptr != ' ' && *ptr != '\0'; ptr++) ;
		if (*ptr == ' ') *ptr++ = '\0';
		if (!*ptr) ptr = NULL;

		ret = 2;
		for (n = 0; CTCPCmds[n].ctcp != NULL; n++) {
			if (g_strcasecmp(CTCPCmds[n].ctcp, event->data) == 0) {
				ctcpevent = g_malloc ( sizeof (CTCPEventRec) );
				ctcpevent->sender = event->nick;
				ctcpevent->target = channame;
				ctcpevent->data = ptr;
				ctcpevent->server = event->server;
				ret = CTCPCmds[n].func( ctcpevent );
				g_free ( ctcpevent );
				break;
			}
		}

		if (ret) {
			irc_chan_print(
				irc_chan_find_in_serv (channame, event->server),
				TLVL_MSG, CTCP_REQ,
				event->nick, ret==1?"":"unknown ctcp ", data,
				channame );
		}
		return;
	}


	if (ischannel(*channame)) {
		/* message to some channel */
		gchar *nick;
		gint nicklen;

		level = TLVL_CHAN;

/*
		if (event->addr != NULL) {
			if (ignore_check(event->server, event->nick, LEVEL_PUBLIC))
				return;
			flood_newmsg(eserver, LEVEL_PUBLIC, event->nick);
		}
*/
		chan = irc_chan_find_in_serv ( channame, event->server );
		nick = chan!=NULL?chan->Server->user->nick:
			event->server->user->nick;
		nicklen = strlen(nick);
		if (strstr(data, nick) != NULL && !isalnum(data[nicklen])) {
			if (chan != NULL &&
			    g_strcasecmp(channame,
					 chan->Window->CurrentChannel->Name)==0)
				sprintf(format, "<%%1%s%%n> %s\n",
					event->nick, data);
			else
				sprintf(format, "<%%1%s%%n:%%2%s%%n> %s\n",
					event->nick, channame, data);
		}
		else {
			if (chan != NULL &&
			    g_strcasecmp(channame,
					 chan->Window->CurrentChannel->Name)==0)
				sprintf(format, "<%s> %s\n", event->nick, data);
			else
				sprintf(format, "<%s:%%2%s%%n> %s\n",
					event->nick, channame, data);
		}
	}
	else {
		/* private message */

/*
		if (event->addr != NULL) {
			if (ignore_check(event->server, event->nick, LEVEL_MSGS))
				return;
			flood_newmsg(eserver, LEVEL_MSGS, esendnick);
		}
*/

		chan = irc_chan_find_in_serv (event->nick, event->server);
		level = TLVL_MSG;
		sprintf(format, FRMT_MSG_RECV,
			event->nick, event->addr == NULL?"":event->addr, data);
		/* if autoquery is on and the dude doesnt already have a query window goin, make one */
//		if (global_settings->autoquery && chan == NULL)
//			irccmd_query(esendnick);

	}
	irc_chan_print (
		irc_chan_find_in_serv (level==TLVL_MSG?event->nick:channame,
			event->server),
		TLVL_MSG, format );

//	g_free ( data );
}

void events_named_notice( EventRec *event )
{
	gchar		*channame,
			*data,
	                *origdata;
	ChannelRec	*chan;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	origdata = data = g_strdup ( event->data );

	channame = get_param(&data); /* Channel or nick name */
	if (*data == ':') data++; /* notice text */

	if (event->nick == NULL) event->nick = "server";

/*
	if (event->addr != NULL) {
		if (ignore_check(eserver, esendnick, LEVEL_NOTICES)) return;
		flood_newmsg(eserver, LEVEL_NOTICES | is_channel(*channame) ? LEVEL_PUBLIC : LEVEL_MSGS, esendnick);
	}
*/

	if (*data == 1) {
		/* ctcp reply */
		gchar *ptr;

		ptr = strchr(++data, 1);
		if (ptr != NULL) *ptr = '\0';

		ctcp_reply(event->nick, data);
	}
	else {
		if (event->addr == NULL) {
			/* notice from server */
			chan = irc_chan_find_in_serv( channame, event->server );
			irc_chan_print ( chan, TLVL_SERVER, FRMT_SNOTICE,
				event->nick, data );
		}
		else {
			/* notice from user */
			chan = irc_chan_find_in_serv( channame,
				event->server);
			irc_chan_print ( chan, TLVL_NOTICE,
				FRMT_NOTICE,
				event->nick, channame, data );
		}
	}

	g_free (origdata);
}

/* Check if nick is joined to any channel in specified window */
static gint nick_joined(WindowRec *win, gchar *nick)
{
	GList *chan;

	g_return_val_if_fail(win != NULL, 0);
	g_return_val_if_fail(nick != NULL, 0);

	GLIST_FOREACH(chan, win->Channels)
		if (irc_nick_find_in_chan((ChannelRec *) chan->data, nick) )
			return 1;

	return 0;
}

void events_named_nick( EventRec *event )
{
	gchar *nick, *oldnick;
	GList *win;

	g_return_if_fail(event != NULL );
	g_return_if_fail(event->data != NULL);

	nick = get_param(&(event->data));

	if (g_strcasecmp(event->nick, event->server->user->nick) == 0) {
		/* You changed your nick */
		oldnick = event->server->user->nick;
		event->server->user->nick = g_strdup(nick);
//		gui_nick_change(eserver, oldnick, nick);
		g_free(oldnick);
/* FIXME: wont say when changing own nick */
//		drawtext(event->server, LEVEL_NOTICE, , event->server->nick);
//		gui_update_statusbar(NULL);
	}
	else {
		/* Someone else changed nick */
		GLIST_FOREACH(win, Windows)
		{
			WindowRec *winrec;
			GList *chan;
			gint found;

			winrec = (WindowRec *) win->data;
			found = 0;
			GLIST_FOREACH(chan, winrec->Channels)
			{
				ChannelRec *rec;

				rec = (ChannelRec *) chan->data;
				if (ischannel(*rec->Name)) {
					/* channel */
					if (!found &&
					    irc_nick_find_in_chan(rec,
					    event->nick ) ) {
						found = 1;
					}
					else {
						/* query */
						gint dcc;

						dcc = *rec->Name == '=';
						if (g_strcasecmp(dcc?rec->Name+1
						    :rec->Name, event->nick)==0) {
							/* change query name */
							if (!found)
								found = 1;
							g_free(rec->Name);
							rec->Name = g_new(gchar,
								strlen(nick)+dcc?2:1);
							if (!dcc)
								strcpy(rec->Name, nick);
							else
								sprintf(rec->Name,
									"=%s", nick);

//							gui_channel_change_name(rec,
//								rec->Name);
						}
					}
				}

				if (found == 1) {
					/* print nick change to window.. */
					irc_window_print(winrec, TLVL_MSG,
						MSG_NICK_CHNGE, event->nick,
						nick);
					found = 2;
				}
			}
		}
//		gui_nick_change(eserver, esendnick, nick);
	}

/*
	GLIST_FOREACH(win, Windows)
	{
		GList *chan;

		GLIST_FOREACH(chan, ((WindowRec *) win->data)->Channels)
		{
			ChannelRec *ch;

			ch = (ChannelRec *) chan->data;
			if (ch->server == eserver)
				change_nick(ch, esendnick, nick);
		}
	}
*/
}

void events_named_join( EventRec *event )
{
	gchar *channame, *tmp;
	ChannelRec *chan;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	channame = get_param(&event->data);
	tmp = strchr(channame, 7); /* ^G does something weird.. */
	if (tmp != NULL) *tmp = '\0';

	chan = irc_chan_find_in_serv ( channame, event->server );

	irc_chan_print ( chan, TLVL_MSG, MSG_CHAN_JOINED, event->nick,
		event->addr, channame );

	if (g_strcasecmp(event->nick, event->server->user->nick) != 0) {

		/* someone else joined channel */
		NickRec *rec;

		if (chan == NULL) return;

		rec = irc_chan_addnick(chan, event->nick);
		rec->host = g_strdup(event->addr);
//		gui_channel_join(chan, esendnick);
        
		return;
	}

	/* you joined .. send some queries to server about channel.. */
	if (!chan) irc_chan_join(event->server->defwindow, channame); /* chan window didnt exist...make it */
	
	tmp = g_new(gchar, strlen(chan->Name)+10);
	sprintf(tmp, "MODE %s", chan->Name);
	irc_cmd_send(event->server, tmp);

	sprintf(tmp, "MODE %s b", chan->Name);
	irc_cmd_send(event->server, tmp);

	sprintf(tmp, "WHO %s", chan->Name);
	irc_cmd_send(event->server, tmp);

	g_free(tmp);
	chan->queries = CHANQUERY_BANLIST | CHANQUERY_WHO | CHANQUERY_MODE;
}

void events_named_part( EventRec *event )
{
	gchar *channame, *reason;
	ChannelRec *chan;
	WindowRec *win;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	get_params(event->data, 2, &channame, &reason);
	chan = irc_chan_find_in_serv( channame, event->server );

	irc_chan_print ( chan, TLVL_MSG, MSG_CHAN_PARTED, event->nick,
		event->addr, channame, reason );

	if (chan == NULL) return;

	if (g_strcasecmp(event->nick, event->server->user->nick) != 0) {
		/* someone else left channel */
	        irc_chan_remnick( chan, event->nick );
//		gui_channel_part(chan, esendnick);
		return;
	}

	win = chan->Window;
//	if (chan == win->CurrentChannel)
//		irc_select_new_channel(chan->window);
//	if (win != CurrentWindow) {
//		irc_window_focus(win);
//		gui_select_channel(curwin, chan);
//	}

	irc_chan_part ( chan );
}

void events_named_ping( EventRec *event )
{
	gint conn;
	gchar *tmp;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	tmp = g_new(gchar, strlen(event->data)+6);
	conn = event->server->connected;
	event->server->connected = 1;
	sprintf(tmp, "PONG %s", event->data);
	irc_cmd_send(event->server, tmp);
	event->server->connected = conn;
	g_free(tmp);
}

void events_named_pong( EventRec *event )
{
	gchar *host, *reply;

	g_return_if_fail ( event != NULL );
	g_return_if_fail ( event->data != NULL);

	get_params(event->data, 2, &host, &reply);

	irc_server_print ( event->server, TLVL_SYSTEM, MSG_PONG, host, reply );
}

static void remove_nick ( ChannelRec *chan, gchar *nick )
{
	irc_chan_remnick ( chan, nick );
}

void events_named_quit( EventRec *event )
{
	gchar *data;
	GList *win;
	WindowRec *winrec;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	data = g_strdup ( event->data );

	if (*data == ':') data++; /* quit message */
	GLIST_FOREACH(win, Windows)
	{
		winrec = (WindowRec *) win->data;
		if (nick_joined(winrec, event->nick)) {
			irc_window_print ( winrec, TLVL_MSG, MSG_QUIT,
				event->nick, event->addr, data );
			g_list_foreach(winrec->Channels, (GFunc) remove_nick,
				event->nick);
		}
	}

}

void events_named_mode( EventRec *event )
{
	gchar *channame, *data;
	ChannelRec *chan;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	data = g_strdup ( event->data );

	channame = get_param(&data);
	if (*data == ':') data++;

	if (!ischannel(*channame)) {
		/* user mode change */
		irc_server_print ( event->server, TLVL_MSG, MSG_USER_MCHNG,
			data, channame );
		g_free (data);
		return;
	}

	/* channel mode change */
	chan = irc_chan_find_in_serv( channame, event->server );
		irc_server_print ( event->server, TLVL_MSG, MSG_CHAN_MCHNG,
			channame, data, event->nick );

	if (chan != NULL) irc_chan_parsemode(chan, data);
}

void events_named_kick( EventRec *event )
{
	gchar *channame, *nick, *reason;
	ChannelRec *chan;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	get_params(event->data, 3, &channame, &nick, &reason);
	chan = irc_chan_find_in_serv( channame, event->server);

	irc_chan_print ( chan, TLVL_MSG, MSG_KICK, nick, channame, event->nick, reason );
	if (chan == NULL) return;

	if (g_strcasecmp(nick, event->server->user->nick) != 0) {
		irc_chan_remnick(chan, nick);
	}
	else {
		irc_chan_part ( chan );
	}

}

void events_named_invite( EventRec *event )
{
	gchar *chan;

	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	get_params(event->data, 2, NULL, &chan);
	if (*chan != '\0')
		irc_server_print ( event->server, TLVL_SYSTEM, MSG_INVITE,
			event->nick, chan );
}

void events_named_new_topic( EventRec *event )
{
	ChannelRec *chan;
	gchar *channame, *topic;

	g_return_if_fail(event != NULL );
	g_return_if_fail(event->data != NULL);

	get_params(event->data, 2, &channame, &topic);
	chan = irc_chan_find_in_serv( channame, event->server );
	if (chan == NULL) return;

	irc_chan_settopic ( chan, topic );

	if (chan->Topic != NULL) {
		irc_chan_print ( chan, TLVL_MSG, MSG_TOPIC_NEW,
			event->nick, chan->Name, chan->Topic );
	}
	else {
		irc_chan_print ( chan, TLVL_MSG, MSG_TOPIC_UNSET,
			event->nick, chan->Name );
	}
}

void events_named_error( EventRec *event )
{
	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	if (*(event->data) == ':') (event->data)++;

	irc_server_print ( event->server, TLVL_ERROR, FRMT_SERROR,
		event->data );	
}

void events_named_wallops( EventRec *event )
{
	g_return_if_fail(event != NULL);
	g_return_if_fail(event->data != NULL);

	if (*(event->data) == ':') (event->data)++;

	irc_server_print ( event->server, TLVL_ERROR, FRMT_WALLOPS,
		event->data );	
}
